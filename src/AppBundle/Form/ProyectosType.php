<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Transformer\DateToStringTransformer;
class ProyectosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new DateToStringTransformer(null, null, 'd/m/Y');

        $builder
            ->add('rq')
            ->add(
                $builder->create('fechaInicio', 'text', array('required'=>false,))
                    ->addModelTransformer($transformer)
            )
            ->add(
                $builder->create('fechaEnvio', 'text')
                    ->addModelTransformer($transformer)
            )
            ->add(
                $builder->create('fechaFin', 'text', array('required'=>false,))
                    ->addModelTransformer($transformer)
            )
            ->add(
                $builder->create('fechaEnvio', 'text')
                    ->addModelTransformer($transformer)
            )
            ->add('creadoPor')
            ->add('descripcion')
            ->add('oc')
            ->add('vigencia', 'choice', array(
                'choices'   => array(
                    '01 DIAS' => "01 DIA",
                    '02 DIAS' => "02 DIAS",
                    '03 DIAS' => "03 DIAS",
                    '04 DIAS' => "04 DIAS",
                    '05 DIAS' => "05 DIAS",
                    '06 DIAS' => "06 DIAS",
                    '07 DIAS' => "07 DIAS",
                    '08 DIAS' => "08 DIAS",
                    '09 DIAS' => "09 DIAS",
                    '10 DIAS' => "10 DIAS",
                    '15 DIAS' => "15 DIAS",
                    '20 DIAS' => "20 DIAS",
                    '25 DIAS' => "25 DIAS",
                    '30 DIAS' => "30 DIAS",
                    '35 DIAS' => "35 DIAS",
                    '40 DIAS' => "40 DIAS",
                    '45 DIAS' => "45 DIAS",
                    '50 DIAS' => "50 DIAS",                    
                    '55 DIAS' => "55 DIAS",
                    '60 DIAS' => "60 DIAS",
                ),
                'multiple'  => false,
            ))
            ->add('condiPago', 'choice', array(
              'choices'   => array(
                  '0' => "POR EVALUACION",
                  '1' => "UNA VEZ CULMINADO EL PROYECTO",
              ),
            ))
            ->add('diasPago', 'text')
            ->add('anticipo', 'text')
            ->add('observacion')
            ->add('statusProyecto')
            ->add('cliente')
            ->add('requisitor')
            ->add('requisitorTlf')
            ->add('asignado')
            ->add('prestaciones')
            ->add('gastoAdmin')
            ->add('utilidad')
            ->add('comida')

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Proyectos',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_proyectos';
    }
}
