<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MaterialesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo')
            ->add('nombre')
            ->add('descripcion')
            ->add('usuarioCrea')
            ->add('usuarioModifica')
            ->add('stock','text')
            ->add('disponibilidad','checkbox', array(
                'label'    => '¿Esta Disponible?',
                'required' => false,
            ))
            ->add('precio1','text', array(
                'required' => false,
            ))
            ->add('precio2','text', array(
                'required' => false,
            ))
            ->add('precio3','text', array(
                'required' => false,
            ))
            ->add('precioVenta','text', array(
                'required' => false,
            ))
            ->add('slug')
            ->add('categoria')
            ->add('almacen')
            ->add('unidad')
            ->add('proveedor1')
            ->add('proveedor2')
            ->add('proveedor3')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Materiales'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_materiales';
    }
}
