<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProyectoApuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('proyecto')
            ->add('miscelaneo')
            ->add('nombre')
            ->add('cantApu')
            ->add('cantMedidaApu', 'integer', array(
                        'attr' => array('min' => 1, )
            ))
            ->add('tipoapu')
            ->add('slug')
            ->add('comentario')
            ->add('status', 'choice', array(
                'choices'   => array(
                    '0' => "NO",
                    '1' => "SI",
                ),
            ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ProyectoApu'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_proyectoapu';
    }
}
