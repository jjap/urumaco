<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MaquinariasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo')
            ->add('nombre')
            ->add('modelo')
            ->add('precioDia','text')
            ->add('disponibilidad','checkbox', array(
                'label'    => '¿Esta Disponible?',
                'required' => false,
            ))
            ->add('proyectoAcual')
            ->add('slug')
            ->add('marca')
            ->add('stock','text')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Maquinarias'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_maquinarias';
    }
}
