<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Transformer\DateToStringTransformer;
class NotaEntregaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new DateToStringTransformer(null, null, 'd/m/Y');
        $builder
            ->add('proyecto')
            ->add('comentario')
            ->add($builder->create('fechaEmision', 'text', array('required'=>false,))->addModelTransformer($transformer))
            ->add($builder->create('fechaPlanta', 'text', array('required'=>false,))->addModelTransformer($transformer))
            ->add('contactoUrumaco')
            ->add('contactoEmpresa')
            ->add('tlfUrumaco')
            ->add('tlfEmpresa')
            ->add('codGuia')
            ->add('codControl')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\NotaEntrega'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_notaentrega';
    }
}
