<?php

namespace AppBundle\Controller;

use AppBundle\Entity\HorarioLaboral;
use AppBundle\Entity\Bitacora;
use AppBundle\Form\HorarioLaboralType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * HorarioLaboral controller.
 *
 * @Route("admin/horariolaboral")
 */
class HorarioLaboralController extends Controller
{

    /*Retorna la variable admin_pool requerida en la plantilla de Sonata*/
    public function getAdmin_pool(){
        return $this->get('sonata.admin.pool');        
    }

    /**
     * Lists all HorarioLaboral entities.
     *
     * @Route("/", name="horariolaboral")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:HorarioLaboral')->findAll();

        return array(
            'entities' => $entities,
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }
    
    /**
     * Creates a new HorarioLaboral entity.
     *
     * @Route("/", name="horariolaboral_create")
     * @Method("POST")
     * @Template("AppBundle:HorarioLaboral:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new HorarioLaboral();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear', $entity->getDescripcion());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Horario Laboral ha sido creado con éxito.');
            ;
            return $this->redirect($this->generateUrl('horariolaboral_edit', array('id' => $entity->getId(),'admin_pool'=> $this->getAdmin_pool(),)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    /**
     * Creates a form to create a HorarioLaboral entity.
     *
     * @param HorarioLaboral $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(HorarioLaboral $entity)
    {
        $form = $this->createForm(new HorarioLaboralType(), $entity, array(
            'action' => $this->generateUrl('horariolaboral_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new HorarioLaboral entity.
     *
     * @Route("/nuevo", name="horariolaboral_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new HorarioLaboral();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    // /**
    //  * Finds and displays a HorarioLaboral entity.
    //  *
    //  * @Route("/{id}", name="horariolaboral_show")
    //  * @Method("GET")
    //  * @Template()
    //  */
    // public function showAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('AppBundle:HorarioLaboral')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find HorarioLaboral entity.');
    //     }

    //     $deleteForm = $this->createDeleteForm($id);

    //     return array(
    //         'entity'      => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //     );
    // }

    /**
     * Displays a form to edit an existing HorarioLaboral entity.
     *
     * @Route("/{id}/editar", name="horariolaboral_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:HorarioLaboral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find HorarioLaboral entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    /**
    * Creates a form to edit a HorarioLaboral entity.
    *
    * @param HorarioLaboral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(HorarioLaboral $entity)
    {
        $form = $this->createForm(new HorarioLaboralType(), $entity, array(
            'action' => $this->generateUrl('horariolaboral_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing HorarioLaboral entity.
     *
     * @Route("/{id}", name="horariolaboral_update")
     * @Method("PUT")
     * @Template("AppBundle:HorarioLaboral:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:HorarioLaboral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find HorarioLaboral entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->AgregarBitacora('Editar', $entity->getDescripcion());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Horario Laboral ha sido modificado con éxito.');
            ;
            return $this->redirect($this->generateUrl('horariolaboral_edit', array('id' => $id,'admin_pool'=> $this->getAdmin_pool(),)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }
    /**
     * Deletes a HorarioLaboral entity.
     *
     * @Route("/{id}", name="horariolaboral_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:HorarioLaboral')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find HorarioLaboral entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('horariolaboral'));
    }

    /**
     * Creates a form to delete a HorarioLaboral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('horariolaboral_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * List Ajax All HorarioLaboral entity.
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_horariolaboral_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:HorarioLaboral');
           
            $objRequestParams = json_decode($request->request->get("requestParams"));

            $query = $repository->getQueryByListadoHorarioLaboral($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $horariolaboral = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:HorarioLaboral:detail-list.html.twig', array(
                'horariolaboral'  => $horariolaboral,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    public function AgregarBitacora($Accion, $referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setReferencia($referencia);
        $entity->setModulo('Horario Laboral');
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }
}
