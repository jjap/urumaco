<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Almacen controller.
 *
 * @Route("/admin/reporte")
 */

class ReportesController extends Controller
{

	public function mostrarPDF($html,$filename,$orientacion = ""){

		$pdf = $this->get('white_october.tcpdf')->create();
        $pdf->SetAuthor('Ing Junior Aguilar');
        $pdf->SetTitle('Urumaco');
        $pdf->SetSubject('TCPDF ejemplo simple');
        //$pdf->SetHeaderData('../../../../../web/bundles/app/img/logo_urumaco.png', PDF_HEADER_LOGO_WIDTH);

        // $pdf->SetHeaderData('../../../../../web/bundles/app/img/logo_urumaco.png', PDF_HEADER_LOGO_WIDTH);

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
        //set margins

        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(10,5,10);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        //set auto page breaks
        //        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetAutoPageBreak(true,15);
        
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //set some language-dependent strings
        //$pdf->setLanguageArray($l);
        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('helvetica', '', 10, '', true);

        if($orientacion == ""){
           $pdf->AddPage();
       }else{
            $pdf->AddPage($orientacion);
       }
        

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        $pdf->Output($filename.'.pdf', 'I');
	}

    /**
     * Lists all Almacen entities.
     *
     * @Route("/", name="reporte")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $admin_pool = $this->get('sonata.admin.pool');
        return $this->render('AppBundle:Reportes:index.html.twig', array(
                'admin_pool'=> $admin_pool,
            )
        );
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/almacen", name="reporte_almacen")
     * @Method("GET")   
     */
    public function almacenPDFAction(){

    	$repository = $this->getDoctrine()->getRepository('AppBundle:Almacen');

    	$almacenes=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
        
              
        return $this->mostrarPDF($html,'almacen');
    	// return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/categorias", name="reporte_categorias")
     * @Method("GET")   
     */
    public function categoriasPDFAction(){

        $repository = $this->getDoctrine()->getRepository('AppBundle:Categorias');

        $categorias=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:categoriasPDF.html.twig',array('categorias' => $categorias));
        
              
        return $this->mostrarPDF($html,'categoria');

        // return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/marcas", name="reporte_marcas")
     * @Method("GET")   
     */
    public function marcasPDFAction(){

        $repository = $this->getDoctrine()->getRepository('AppBundle:Marcas');

        $marcas=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:marcasPDF.html.twig',array('marcas' => $marcas));
        
              
        return $this->mostrarPDF($html,'marca');

        //return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/modelos", name="reporte_modelos")
     * @Method("GET")   
     */
    public function modelosPDFAction(){

        $repository = $this->getDoctrine()->getRepository('AppBundle:Modelos');

        $modelos=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:modelosPDF.html.twig',array('modelos' => $modelos));
        
              
        return $this->mostrarPDF($html,'modelo');

        //return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/unidades", name="reporte_unidades")
     * @Method("GET")   
     */
    public function unidadesPDFAction(){

        $repository = $this->getDoctrine()->getRepository('AppBundle:Unidades');

        $unidades=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:unidadesPDF.html.twig',array('unidades' => $unidades));
        
              
        return $this->mostrarPDF($html,'unidad');

        //return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/maquinarias", name="reporte_maquinarias")
     * @Method("GET")   
     */
    public function maquinariasPDFAction(){

        $repository = $this->getDoctrine()->getRepository('AppBundle:Maquinarias');

        $maquinarias=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:maquinariasPDF.html.twig',array('maquinarias' => $maquinarias));
        
              
        return $this->mostrarPDF($html,'maquinaria');

        //return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/proveedores", name="reporte_proveedores")
     * @Method("GET")   
     */
    public function proveedoresPDFAction(){

        $repository = $this->getDoctrine()->getRepository('AppBundle:Proveedores');

        $proveedores=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:proveedoresPDF.html.twig',array('proveedores' => $proveedores));
        
              
        return $this->mostrarPDF($html,'proveedor');

        //return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report Proyecto
     *
     * @Route("/proyectos/{id}", name="reporte_resumenApu")
     * @Method("GET")   
     */
    public function proyectosPDFAction($id){
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('AppBundle:Proyectos');
        $proyecto=$repository->find($id);
        $suma=$this->sumatoria($id);
        $apus = $em->getRepository('AppBundle:ProyectoAPU')->findByProyecto($id);

        $html = $this->renderView('AppBundle:Reportes:resumenApuPDF.html.twig',array('proyecto' => $proyecto,'Proyecto' => $proyecto, 'apus' => $apus, 'suma'=>$suma));

        return $this->mostrarPDF($html,'Proyecto_General','L');


        // return  $this->render('AppBundle:Reportes:proyectosGeneralPDF.html.twig',array('proyecto' => $proyectos, 'apus' => $apus, 'suma'=>$suma));
       
    }

    /**
     * Report Proyecto Apu
     *
     * @Route("/proyectositem/{apu_id}/{proyecto_id}/{num}/{flag}", name="reporte_proyectoapu")
     * @Method("GET")   
     */
    public function proyectosApuDetallePDFAction($apu_id, $proyecto_id, $num,$flag=0){
        $ProyectoApuDetalle = $this->getDoctrine()->getRepository('AppBundle:ProyectoApuDetalle')->findBy(array('proyectoapu'=>$apu_id));
        $Apu = $this->getDoctrine()->getRepository('AppBundle:ProyectoAPU')->find($apu_id); 
        $Proyecto = $this->getDoctrine()->getRepository('AppBundle:Proyectos')->find($proyecto_id);
        $mates = $this->query("SELECT a.id as id, b.abv as abv FROM materiales a, unidades b WHERE a.unidad_id=b.id");

        $Materiales=[];
        foreach ($mates as $mate) {
            $Materiales[$mate['id']]=$mate['abv'];
        }
        $html = $this->renderView('AppBundle:Reportes:proyectosApuPDF.html.twig',array('materiales'=> $Materiales, 'num'=> $num, 'Apu'=> $Apu, 'ProyectoApuDetalle' => $ProyectoApuDetalle, 'Proyecto'=>$Proyecto));
              
        if ($flag==0) {
            return $this->mostrarPDF($html,'Proyecto_Apu');
        }
        else{
            $response = new JsonResponse();
            $content  = $this->render('AppBundle:Reportes:proyectosApuPDF.html.twig',array('materiales'=> $Materiales, 'num'=> $num, 'Apu'=> $Apu, 'ProyectoApuDetalle' => $ProyectoApuDetalle, 'Proyecto'=>$Proyecto));
            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
            return $content;
        }
    }

    /**
     * Report Proyecto All Apu
     *
     * @Route("/proyectosAllItems/{proyecto_id}", name="reporte_proyectoallapu")
     * @Method("GET")   
     */
    public function proyectosAllItemsAction($proyecto_id)
    {
        $pdf = $this->get('white_october.tcpdf')->create();
        $pdf->SetAuthor('Junior Aguilar');
        $pdf->SetTitle('Urumaco');
        $pdf->SetSubject('Reporte Completo de Proyectos');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(10,5,10);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        // set auto page breaks
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetAutoPageBreak(true,15);
        
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings
        // $pdf->setLanguageArray($l);
        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('helvetica', '', 10, '', true);

        
        $Proyecto = $this->getDoctrine()->getRepository('AppBundle:Proyectos')->find($proyecto_id); 
        
        $APU = $this->getDoctrine()->getRepository('AppBundle:ProyectoAPU')->findByProyecto($proyecto_id); 

        $num=1;
        foreach ($APU as $apu) {
            $html="";
            $html=$this->proyectosApuDetallePDFAction($apu->getId(), $proyecto_id, $num,$flag=1);
            $num++;
            $pdf->AddPage();       
            $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html->getContent(), $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        }

        $pdf->AddPage();
        $html= $this->render('AppBundle:Reportes:reporteAllApu.html.twig',array('Apus'=> $APU, 'Proyecto'=>$Proyecto, 'suma'=>$this->sumatoria2($proyecto_id)));
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html->getContent(), $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);        
        $pdf->Output("Reporte Completo".'.pdf', 'I');       
    }

    public function sumatoria($proyecto_id){
        
        $sql="SELECT a.tipoActividad, SUM(a.cantidad *a.dias * costo_unidad) AS costo, COUNT(a.id) AS canti_item, SUM(a.dias) AS dias, SUM(a.dias*a.cantidad) AS comida FROM proyectoapudetalle a WHERE a.proyectoapu IN (SELECT b.id FROM proyectoapu b WHERE b.proyecto=$proyecto_id) GROUP BY a.tipoActividad";
        
        return $this->query($sql);
    }

    public function sumatoria2($proyecto_id){
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT a.tipoActividad, SUM(a.cantidad *a.dias * costo_unidad) AS costo, COUNT(a.id) AS canti_item, SUM(a.dias) AS dias, SUM(a.dias*a.cantidad) AS comida FROM proyectoapudetalle a WHERE a.proyectoapu IN (SELECT b.id FROM proyectoapu b WHERE b.proyecto=$proyecto_id) GROUP BY a.tipoActividad";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function query($sql){
        $em = $this->getDoctrine()->getManager();
        
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * Report Proyecto Apu
     *
     * @Route("/proyectositemIndividual/{apu_id}/{proyecto_id}/{num}", name="reporte_proyectoapuIndividual")
     * @Method("GET")   
     */
    public function proyectosApuIndividualAction($apu_id, $proyecto_id, $num,$flag=0){
        $ProyectoApuDetalle = $this->getDoctrine()->getRepository('AppBundle:ProyectoApuDetalle')->findBy(array('proyectoapu'=>$apu_id));
        $Apu = $this->getDoctrine()->getRepository('AppBundle:ProyectoAPU')->find($apu_id); 
        $Proyecto = $this->getDoctrine()->getRepository('AppBundle:Proyectos')->find($proyecto_id);
        $mates = $this->query("SELECT a.id as id, b.abv as abv FROM materiales a, unidades b WHERE a.unidad_id=b.id");

        $Materiales=[];
        foreach ($mates as $mate) {
            $Materiales[$mate['id']]=$mate['abv'];
        }
        $html = $this->renderView('AppBundle:Reportes:ApuIndividual.html.twig',array('materiales'=> $Materiales, 'num'=> $num, 'Apu'=> $Apu, 'ProyectoApuDetalle' => $ProyectoApuDetalle, 'Proyecto'=>$Proyecto));
              
        if ($flag==0) {
            return $this->mostrarPDF($html,'Proyecto_Apu');
        }
        else{
            return $this->render('AppBundle:Reportes:ApuIndividual.html.twig',array('materiales'=> $Materiales, 'num'=> $num, 'Apu'=> $Apu, 'ProyectoApuDetalle' => $ProyectoApuDetalle, 'Proyecto'=>$Proyecto));
        }
    }


    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/calculoIndividual/{id_apu}/{item}", name="calculoIndividual")
     * @Method("GET")
     * @Template()
     */
    public function calculoIndividualAction($id_apu,$item)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:ProyectoAPU')->find($id_apu);

        $proyecto = $em->getRepository('AppBundle:Proyectos')->find($entity->getProyecto());

        return $this->render("AppBundle:Reportes:calculoIndividualApu.html.twig",
            array(
                'entity' => $entity,
                'suma'=> $this->sumatoriaApu($entity->getId()),
                'Proyecto'=> $proyecto,
                'item' => $item,
            )
        );


    }

    public function sumatoriaApu($apu_id){
        $em  = $this->getDoctrine()->getManager();
        $sql = " SELECT a.tipoActividad, 
                        b.cantApu AS repetir, 
                        SUM(a.cantidad *a.dias * costo_unidad) AS costo, 
                        COUNT(a.id) AS canti_item,
                        SUM(a.dias) AS dias,
                        SUM(a.dias*a.cantidad) AS comida
                        FROM proyectoapudetalle a, proyectoapu b
                        WHERE a.proyectoapu =$apu_id AND b.id=$apu_id
                        GROUP BY a.tipoActividad";        
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * Report all Almacen entities.
     *
     * @Route("/materiales", name="reporte_materiales")
     * @Method("GET")   
     */
    public function materialesPDFAction(){

        $repository = $this->getDoctrine()->getRepository('AppBundle:Materiales');

        $materiales=$repository->findAll();

        $html = $this->renderView('AppBundle:Reportes:materialesPDF.html.twig',array('materiales' => $materiales));
        
              
        return $this->mostrarPDF($html,'materiales','L');

        //return  $this->render('AppBundle:Reportes:almacenPDF.html.twig',array('almacenes' => $almacenes));
       
    }

    /**
     * Report Proyecto Apu
     *
     * @Route("/proyectosmateriales/{proyecto_id}", name="reporte_proyectomateriales")
     * @Method("GET")   
     */
    public function proyectosMaterialesAction($proyecto_id){
        $em = $this->getDoctrine()->getManager();
        $Proyecto   = $this->getDoctrine()->getRepository('AppBundle:Proyectos')->find($proyecto_id); 
        $apus = $em->getRepository('AppBundle:ProyectoAPU')->findByProyecto($proyecto_id);

        $sql="SELECT d.id, e.miscelaneo as misc, a.descripcion AS Material, b.categoria_id AS categoria, c.nombre AS unidad, a.cantidad, b.precioVenta FROM proyectoapudetalle a, materiales b, unidades c, proyectos d, proyectoapu e WHERE tipoActividad=3 AND a.idActividad = b.id AND b.unidad_id = c.id AND e.id=a.proyectoapu and e.proyecto=$proyecto_id group by e.id, a.idActividad";
        $sql2="SELECT c.id, c.nombre FROM categorias c WHERE c.id IN (SELECT b.categoria_id FROM proyectoapudetalle a, materiales b WHERE a.tipoActividad=3 AND a.idActividad = b.id  AND a.proyectoapu IN (SELECT e.id FROM proyectoapu e WHERE proyecto=$proyecto_id) )";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $materiales = $stmt->fetchAll();
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $categorias = $stmt->fetchAll();

        $mat= array();
        foreach ($materiales as $mate) {
            $mat[]=array(
                'nombre'  => $mate['Material'],
                'categoria'=>$mate['categoria'],
                'cantidad'=> $mate['cantidad'],
                'unidad'  => $mate['unidad'],
                'precioVenta' => $mate['precioVenta'],
                'misc'    => $mate['misc'],
                );
        }

        $cat= array();
        foreach ($categorias as $cate) {
            $cat[]=array('id'=> $cate['id'], 'nombre'=> $cate['nombre']);           
        }


        $html= $this->render('AppBundle:Reportes:proyectoMateriales.html.twig', array('Proyecto' => $Proyecto, 'materiales'=>$mat, 'categorias'=> $cat));
              
        return $this->mostrarPDF($html->getContent(),'Proyecto_Apu');
        //return $html;
    }

    /**
     * Report Proyecto Apu
     *
     * @Route("/proyectosmaterialesApu/{apu_id}-{num}", name="reporte_proyectomaterialesapu")
     * @Method("GET")   
     */
    public function proyectoMaterialesApuAction($apu_id, $num){
        $em = $this->getDoctrine()->getManager();
        $apu= $em->getRepository('AppBundle:ProyectoAPU')->find($apu_id);
    
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT b.nombre AS Material, b.categoria_id AS categoria, c.nombre AS unidad, a.cantidad, b.precioVenta FROM proyectoapudetalle a, materiales b, unidades c WHERE tipoActividad=3 AND a.idActividad = b.id AND b.unidad_id = c.id AND a.proyectoapu=$apu_id";
        $sql2="SELECT c.id, c.nombre  FROM categorias c WHERE c.id IN (SELECT b.categoria_id FROM proyectoapudetalle a, materiales b WHERE a.tipoActividad=3 AND a.idActividad = b.id  AND a.proyectoapu=$apu_id)";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $materiales = $stmt->fetchAll();
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $categorias = $stmt->fetchAll();

        $mat= array();
        foreach ($materiales as $mate) {
            $mat[]=array(
                'nombre'  => $mate['Material'],
                'categoria'=>$mate['categoria'],
                'cantidad'=> $mate['cantidad'],
                'unidad'  => $mate['unidad'],
                'precioVenta' => $mate['precioVenta'],
                );
        }

        $cat= array();
        foreach ($categorias as $cate) {
            $cat[]=array('id'=> $cate['id'], 'nombre'=> $cate['nombre']);           
        }

        $html= $this->render('AppBundle:Reportes:proyectoMaterialesApu.html.twig',array('nombre'=> $apu->getNombre(), 'num'=> $num, 'categorias' => $cat, 'materialesApu' => $mat));
        return $html;
    }

    /**
     * Report Proyecto Apu
     *
     * @Route("/proyectosmanoobra/{proyecto_id}", name="reporte_proyectomanoobras")
     * @Method("GET")   
     */
    public function proyectoManoObraAction($proyecto_id){
        $em = $this->getDoctrine()->getManager();
        $Proyecto   = $this->getDoctrine()->getRepository('AppBundle:Proyectos')->find($proyecto_id); 
        $apus = $em->getRepository('AppBundle:ProyectoAPU')->findByProyecto($proyecto_id);

        $sql="SELECT c.descripcion as descrip, sum(c.cantidad * dias) as cantidad, c.costo_unidad as precioDia
                FROM proyectoapu a, proyectoapu b, proyectoapudetalle c
                where a.id = b.proyecto and b.id = c.proyectoapu and tipoActividad = 1 and a.id = $proyecto_id
                group by c.idActividad
                order by c.descripcion ASC;";
      
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $manoObra = $stmt->fetchAll();

        $mo= array();
        foreach ($manoObra as $manoO) {
            $mo[]=array(
                'descrip'  => $manoO['descrip'],
                'cantidad'=> $manoO['cantidad'],
                'precioDia' => $manoO['precioDia'],
                );
        }

        $html= $this->render('AppBundle:Reportes:proyectoManoObra.html.twig', array('Proyecto' => $Proyecto, 'manoObra'=>$mo));
              
        return $this->mostrarPDF($html->getContent(),'Proyecto_Apu');
        //return $html;
    }

    /**
     * Report Proyecto Apu
     *
     * @Route("/proyectosmaquinarias/{proyecto_id}", name="reporte_proyectomaquinarias")
     * @Method("GET")   
     */
    public function proyectoMaquinariasAction($proyecto_id){
        $em = $this->getDoctrine()->getManager();
        $Proyecto   = $this->getDoctrine()->getRepository('AppBundle:Proyectos')->find($proyecto_id); 
        $apus = $em->getRepository('AppBundle:ProyectoAPU')->findByProyecto($proyecto_id);

        $sql="SELECT c.descripcion as descrip, sum(c.cantidad * dias) as cantidad, c.costo_unidad as precioDia
                FROM proyectoapu a, proyectoapu b, proyectoapudetalle c
                where a.id = b.proyecto and b.id = c.proyectoapu and tipoActividad = 2 and a.id = $proyecto_id
                group by c.idActividad
                order by c.descripcion ASC;";
      
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $manoObra = $stmt->fetchAll();

        $mo= array();
        foreach ($manoObra as $manoO) {
            $mo[]=array(
                'descrip'  => $manoO['descrip'],
                'cantidad'=> trim($manoO['cantidad']),
                'precioDia' => $manoO['precioDia'],
                );
        }


        $html= $this->render('AppBundle:Reportes:proyectoMaquinarias.html.twig', array('Proyecto' => $Proyecto, 'manoObra'=>$mo));
              
              
        return $this->mostrarPDF($html->getContent(),'Proyecto_Apu');
        //return $html;
    }

    /**
     * Report Proyecto Apu
     *
     * @Route("/notaentrega/{nota}/{proyecto}", name="reporte_notaentrega")
     * @Method("GET")   
     */
    public function notaentregaAction($nota, $proyecto){
        $em = $this->getDoctrine()->getManager();
        $proyect  = $em->getRepository('AppBundle:Proyectos')->find($proyecto);
        $cliente  = $proyect->getCliente();
        $detalles = $em->getRepository('AppBundle:DetalleNotaEntrega')->findByNotaentrega($nota);
        $nota     = $em->getRepository('AppBundle:NotaEntrega')->find($nota);


        $html= $this->render('AppBundle:Reportes:notaentrega.html.twig', array('proyecto' => $proyect, 'cliente'=> $cliente, 'detalles' => $detalles, 'nota' => $nota));
              
              
        return $this->mostrarPDF($html->getContent(),'Nota_de_Entrega');
        //return $html;
    }

}