<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Materiales;
use AppBundle\Entity\Bitacora;
use AppBundle\Form\MaterialesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Materiales controller.
 *
 * @Route("admin/materiales")
 */
class MaterialesController extends Controller
{

    /**
     * Lists all Materiales entities.
     *
     * @Route("/", name="materiales")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM AppBundle:Materiales a";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        // parameters to template
       // return $this->render('AppBundle:Almacen:lista.html.twig', );

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'admin_pool'=> $admin_pool,
            'materiales' => $pagination,
        );
    }
    /**
     * Creates a new Materiales entity.
     *
     * @Route("/", name="materiales_create")
     * @Method("POST")
     * @Template("AppBundle:Materiales:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Materiales();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // $usuario= $this->get('security.context')->getToken()->getUser();
            // $entity->setusuarioCrea();
            // $entity->setusuarioModifica();
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear',$entity->getNombre());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Material ha sido creado con éxito.');
            ;
            return $this->redirect($this->generateUrl('materiales_edit', array('id' => $entity->getId())));
        }

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Creates a form to create a Materiales entity.
     *
     * @param Materiales $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Materiales $entity)
    {
        $form = $this->createForm(new MaterialesType(), $entity, array(
            'action' => $this->generateUrl('materiales_create'),
            'method' => 'POST',
        ));

        // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Materiales entity.
     *
     * @Route("/nuevo", name="materiales_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Materiales();
        $form   = $this->createCreateForm($entity);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Finds and displays a Materiales entity.
     *
     * @Route("/{id}", name="materiales_show")
     * @Method("GET")
     * @Template()
     */
    // public function showAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('AppBundle:Materiales')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find Materiales entity.');
    //     }

    //     $deleteForm = $this->createDeleteForm($id);

    //     return array(
    //         'entity'      => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //     );
    // }

    /**
     * Displays a form to edit an existing Materiales entity.
     *
     * @Route("/editar/{id}", name="materiales_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Materiales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Materiales entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
    * Creates a form to edit a Materiales entity.
    *
    * @param Materiales $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Materiales $entity)
    {
        $form = $this->createForm(new MaterialesType(), $entity, array(
            'action' => $this->generateUrl('materiales_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        // $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Materiales entity.
     *
     * @Route("/{id}", name="materiales_update")
     * @Method("PUT")
     * @Template("AppBundle:Materiales:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Materiales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Materiales entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->AgregarBitacora('Editar', $entity->getNombre());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Material ha sido modificado con éxito.');
            ;
            return $this->redirect($this->generateUrl('materiales_edit', array('id' => $id)));
        }

        $admin_pool = $this->get('sonata.admin.pool');


        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $admin_pool,
        );
    }
    /**
     * Deletes a Materiales entity.
     *
     * @Route("/{id}", name="materiales_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Materiales')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Materiales entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('materiales'));
    }

    /**
     * Creates a form to delete a Materiales entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('materiales_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Deletes a Almacen entity.
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_materiales_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:Materiales');

            $objRequestParams = json_decode($request->request->get("requestParams"));

            $query = $repository->getQueryByListadoMateriales($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $materiales = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:Materiales:detail-list.html.twig', array(
                'materiales'  => $materiales,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    // /**
    //  * Lists all Almacen entities.
    //  *
    //  * @Route("/listado", name="materiales_listado")
    //  * @Method("GET")
    //  * @Template()
    //  */
    // public function listAction(Request $request)
    // {
    //     $em    = $this->get('doctrine.orm.entity_manager');
    //     $dql   = "SELECT a FROM AppBundle:Materiales a";
    //     $query = $em->createQuery($dql);

    //     $paginator  = $this->get('knp_paginator');
    //     $pagination = $paginator->paginate(
    //         $query,
    //         $request->query->getInt('page', 1)/*page number*/,
    //         10/*limit per page*/
    //     );

    //     // parameters to template
    //     return $this->render('AppBundle:Materiales:lista.html.twig', array('pagination' => $pagination));
    // }

    public function AgregarBitacora($Accion,$referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setReferencia($referencia);
        $entity->setModulo('Materiales');
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     *
     * @Route("/delete/{id}", name="materialdelete", options={"expose"=true} )
     * @Method("GET")
     */
    public function materialdeleteAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('AppBundle:Materiales')->find($id);
      $em->remove($entity);
      $em->flush();
      $response = new JsonResponse();

      return $response->setData(array(
              'status' => true,
              'content' => true,
              'error' => null
      ));

    }



}
