<?php

namespace AppBundle\Controller;

use AppBundle\Entity\StatusProyecto;
use AppBundle\Entity\Bitacora;
use AppBundle\Form\StatusProyectoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * StatusProyecto controller.
 *
 * @Route("/admin/statusproyecto")
 */
class StatusProyectoController extends Controller
{
    /*Retorna la variable admin_pool requerida en la plantilla de Sonata*/
    public function getAdmin_pool(){
        return $this->get('sonata.admin.pool');        
    }

    /**
     * Lists all StatusProyecto entities.
     *
     * @Route("/", name="statusproyecto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:StatusProyecto')->findAll();

        return array(
            'entities' => $entities,
            "admin_pool"=>$this->getAdmin_pool(),
        );
    }
    /**
     * Creates a new StatusProyecto entity.
     *
     * @Route("/", name="statusproyecto_create")
     * @Method("POST")
     * @Template("AppBundle:StatusProyecto:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new StatusProyecto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear', $entity->getName());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Estatus de Proyecto ha sido creado con éxito.');
            ;
            return $this->redirect($this->generateUrl('statusproyecto_edit', array('id' => $entity->getId(),"admin_pool"=>$this->getAdmin_pool(),)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            "admin_pool"=>$this->getAdmin_pool(),
        );
    }

    /**
     * Creates a form to create a StatusProyecto entity.
     *
     * @param StatusProyecto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(StatusProyecto $entity)
    {
        $form = $this->createForm(new StatusProyectoType(), $entity, array(
            'action' => $this->generateUrl('statusproyecto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new StatusProyecto entity.
     *
     * @Route("/nuevo", name="statusproyecto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new StatusProyecto();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            "admin_pool"=>$this->getAdmin_pool(),
        );
    }

    // /**
    //  * Finds and displays a StatusProyecto entity.
    //  *
    //  * @Route("/{id}", name="statusproyecto_show")
    //  * @Method("GET")
    //  * @Template()
    //  */
    // public function showAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('AppBundle:StatusProyecto')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find StatusProyecto entity.');
    //     }

    //     $deleteForm = $this->createDeleteForm($id);

    //     return array(
    //         'entity'      => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //         "admin_pool"=>$this->getAdmin_pool(),
    //     );
    // }

    /**
     * Displays a form to edit an existing StatusProyecto entity.
     *
     * @Route("/{id}/editar", name="statusproyecto_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StatusProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StatusProyecto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            "admin_pool"=>$this->getAdmin_pool(),
        );
    }

    /**
    * Creates a form to edit a StatusProyecto entity.
    *
    * @param StatusProyecto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(StatusProyecto $entity)
    {
        $form = $this->createForm(new StatusProyectoType(), $entity, array(
            'action' => $this->generateUrl('statusproyecto_update', array('id' => $entity->getId(),"admin_pool"=>$this->getAdmin_pool(),)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing StatusProyecto entity.
     *
     * @Route("/{id}", name="statusproyecto_update")
     * @Method("PUT")
     * @Template("AppBundle:StatusProyecto:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StatusProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StatusProyecto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->AgregarBitacora('Editar', $entity->getName());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Estatus de Proyecto ha sido modificado con éxito.');
            ;
            return $this->redirect($this->generateUrl('statusproyecto_edit', array('id' => $id,"admin_pool"=>$this->getAdmin_pool(),)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a StatusProyecto entity.
     *
     * @Route("/{id}", name="statusproyecto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:StatusProyecto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find StatusProyecto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('statusproyecto'));
    }

    /**
     * Creates a form to delete a StatusProyecto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('statusproyecto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * List Ajax All StatusProyecto entity.
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_statusproyecto_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:StatusProyecto');
           
            $objRequestParams = json_decode($request->request->get("requestParams"));

            $query = $repository->getQueryByListadoStatusproyecto($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $statusproyecto = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:StatusProyecto:detail-list.html.twig', array(
                'statusproyecto'  => $statusproyecto,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    public function AgregarBitacora($Accion, $referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setModulo('Status de Proyecto');
        $entity->setReferencia($referencia);
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }

}