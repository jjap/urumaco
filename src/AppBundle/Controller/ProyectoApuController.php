<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProyectoAPU;
use AppBundle\Entity\Proyectos;
use AppBundle\Entity\ProyectoApuDetalle;
use AppBundle\Entity\Bitacora;
use Doctrine\ORM\EntityRepository;
use AppBundle\Form\ProyectoApuType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * ProyectoApu controller.
 *
 * @Route("/admin/proyectos/apu")
 */
class ProyectoApuController extends Controller
{

	/*Retorna la variable admin_pool requerida en la plantilla de Sonata*/
    public function getAdmin_pool(){
        return $this->get('sonata.admin.pool');        
    }

	/**
     *Lista todos los apu de un proyecto
     *
     * @Route("/rq/{slug}", name="proyectoapu_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        try {
        	
        	$apus = $em->getRepository('AppBundle:ProyectoAPU')->findByProyecto($slug);
        	$proyecto = $em->getRepository('AppBundle:Proyectos')->find($slug);

		    return $this->render('AppBundle:ProyectoApu:index.html.twig', array(
                'admin_pool'=>$this->getAdmin_pool(),
                'apus'  => $apus,
                'proyecto'=> $proyecto,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));
        } catch (Exception $e) {
        	
        }       
    }

    /**
     * Creates a new ProyectoApu entity.
     * @Route("/create/{id_proyecto}", name="proyectoapu_create")
     * @Method("POST")
     * @Template("AppBundle:ProyectoApu:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ProyectoAPU();
        $entity->setProyecto($request->get("id_proyecto")); 

        $form = $this->createCreateForm($entity, $request->get("id_proyecto"));
        $form->handleRequest($request);


        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setMiscelaneo($entity->getMiscelaneo());
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear',$entity->getMiscelaneo());

            return $this->redirect($this->generateUrl('proyectoapu_show', array('slug' => $request->get("id_proyecto"))));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

	/**
     * Creates a form to create a ProyectoApu entity.
     *
     * @param ProyectoApu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProyectoApu $entity, $id_proyecto)
    {
        
        $form = $this->createForm(new ProyectoApuType(), $entity, array(
            'action' => $this->generateUrl('proyectoapu_create', array("id_proyecto"=>$id_proyecto)),
            'method' => 'POST',
        ));

        // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ProyectoApu entity.
     *
     * @Route("/nuevo/{id_proyecto}", name="proyectoapu_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($id_proyecto)
    {
        $entity = new ProyectoApu();
        $entity->setProyecto($id_proyecto);
        $form   = $this->createCreateForm($entity, $id_proyecto);
        $admin_pool = $this->get('sonata.admin.pool');
        $response = new JsonResponse();

        $content= $this->renderView("AppBundle:ProyectoApu:form.html.twig",
            array(
            'entity' => $entity,
            'accion' => "Crear",
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
            )
        );

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

    }

    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/edit/{id}/{item}", name="proyectoapu_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id,$item)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:ProyectoAPU')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proyectos entity.');
        }

        $editForm = $this->createEditForm($entity);      
        $admin_pool = $this->get('sonata.admin.pool');
        $response = new JsonResponse();
        $proyecto = $em->getRepository('AppBundle:Proyectos')->find($entity->getProyecto());

        $content= $this->renderView("AppBundle:ProyectoApu:form.html.twig",
            array(
                'entity' => $entity,
                'accion' => "Editar",
                'form'   => $editForm->createView(),
                'admin_pool'=> $this->getAdmin_pool(),
                "id_apu"=> $id,
                'suma'=> $this->sumatoriaApu($entity->getId()),
                'proyecto'=> $proyecto,
                'item' => $item,
            )
        );

        return $response->setData(array(
                'status'  => true,
                'content' => $content,
                'error'   => null,
            ));
    }

    /**
    * Creates a form to edit a Proyectos entity.
    *
    * @param Proyectos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ProyectoApu $entity)
    {
        $form = $this->createForm(new ProyectoApuType(), $entity, array(
            'action' => $this->generateUrl('proyectoapu_update', array('id' => $entity->getId(),'admin_pool'=> $this->getAdmin_pool(),)),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/update/{id}", name="proyectoapu_update")
     * @Method("PUT")
     * @Template("AppBundle:ProyectoApu:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ProyectoAPU')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proyectos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->AgregarBitacora('Editar',$entity->getMiscelaneo());

            return $this->redirect($this->generateUrl('proyectoapu_show', array('slug' => $entity->getProyecto(), 'admin_pool'=> $this->getAdmin_pool(),)));
            //return $this->redirect($this->generateUrl('proyectoapu_edit', array('id' => $id,'admin_pool'=> $this->getAdmin_pool(),)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/proyectoApuAddDetails/{formAct}", name="proyectoApuAddDetails", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function proyectoApuAddDetailsAction($formAct)
    {
        if ($formAct=="maquinaria") {
            $detalle="Agregar Maquinaria";
            $opt=1;
        }elseif ($formAct=="manoObra") {
            $detalle="Agregar Mano de Obra";
            $opt=2;
        }else{
            $detalle="Agregar Material";
            $opt=3;
        }

        $form = $this->createFormBuilder()
          ->add('manoObra', 'entity',array(
              'class' => 'AppBundle:ManoObra',
              'empty_data'  => 0,
              'multiple' => true,
              'label' => 'Seleccionar Mano de Obra',
              'mapped' => false,
              'placeholder' => 'Elige una Mano de Obra',
              'property'=>'listadoManoObra',
              'required' => false,
              'query_builder' => function (EntityRepository $er)
              {
               return $er
                       ->createQueryBuilder('sc')
                       ->groupBy('sc.id')
                       ->orderBy('sc.descripcion');;
              },
            ))
          ->add('costoManoObra', 'entity',array(
              'class' => 'AppBundle:ManoObra',
              'empty_data'  => 0,
              'multiple' => true,
              'label' => 'Seleccionar Mano de Obra',
              'mapped' => false,
              'placeholder' => '0',
              'property'=>'listadoCostoManoObra',
              'required' => false,
              'query_builder' => function (EntityRepository $er)
              {
               return $er
                       ->createQueryBuilder('sc')
                       ->groupBy('sc.id')
                       ->orderBy('sc.descripcion');
              },
          ))
          ->add('maquinaria', 'entity',array(
            'class' => 'AppBundle:Maquinarias',
            'empty_data'  => 0,
            'multiple' => true,
            'label' => 'Seleccionar Maquinaria',
            'mapped' => false,
            'placeholder' => 'Elige una Maquinaria',
            'property'=>'listadoMaquinarias',
            'required' => false,
            'query_builder' => function (EntityRepository $er)
            {
              return $er
                     ->createQueryBuilder('sc')
                     ->groupBy('sc.id')
                     ->orderBy('sc.nombre');
            },
          ))
          ->add('costoMaquinaria', 'entity',array(
                   'class' => 'AppBundle:Maquinarias',
                   'empty_data'  => 0,
                   'multiple' => true,
                   'label' => 'Seleccionar Maquinaria',
                   'mapped' => false,
                   'placeholder' => '0',
                   'property'=>'listadoCostoMaquinarias',
                   'required' => false,
                   'query_builder' => function (EntityRepository $er)
                   {
                       return $er
                               ->createQueryBuilder('sc')
                               ->groupBy('sc.id')
                               ->orderBy('sc.nombre');
                   },
              ))
          ->add('materiales', 'entity',array(
                   'class' => 'AppBundle:Materiales',
                   'empty_data'  => 0,
                   'multiple' => true,
                   'label' => 'Seleccionar Material',
                   'mapped' => false,
                   'placeholder' => 'Elige un Material',
                   'property'=>'listadoMateriales',
                   'required' => false,
                   'query_builder' => function (EntityRepository $er)
                   {
                       return $er
                               ->createQueryBuilder('sc')
                               ->groupBy('sc.id')
                               ->orderBy('sc.nombre');                           
                   },
              ))
          ->add('costoMateriales', 'entity',array(
                   'class' => 'AppBundle:Materiales',
                   'empty_data'  => 0,
                   'label' => 'Seleccionar Material',
                   'mapped' => false,
                   'placeholder' => '0',
                   'property'=>'listadoCostoMateriales',
                   'required' => false,
                   'query_builder' => function (EntityRepository $er)
                   {
                       return $er
                               ->createQueryBuilder('sc')
                               ->groupBy('sc.id')
                               ->orderBy('sc.nombre');
                   },
              ))
          ->add("cantidad","text")
          ->add("costo","text")
          ->add("dia","text")
          ->getForm();


        $response = new JsonResponse();

        $content= $this->renderView("AppBundle:ProyectoApu:formDetailsApu.html.twig",
                    array(
                'form'   => $form->createView(),
                'detalle'=> $detalle,
                "opt"=> $opt,
            )
         );

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/proyectoApuCreateAddDetails/{num_apu}--{tipo}--{actividad}--{cantidad}--{costo}--{dias}--{descrip}", name="proyectoapu_createAddDetail", options={"expose"=true})
     * @Method("get")
     */
    public function AgregarDetalleApuAction(Request $request, $num_apu, $tipo, $actividad, $cantidad, $costo, $dias, $descrip )
    {

        $patrones = array();
        $patrones[0] = '/-/';
        $sustituciones = array();
        $sustituciones[0] = '/';
        $descrip = preg_replace($patrones, $sustituciones, $descrip);

        if ($tipo==3) {
            $actividad=split(',',$actividad);
            $em = $this->getDoctrine()->getManager();
            for ($i=0; $i< count($actividad) ; $i++) {
                $query = $em->getRepository('AppBundle:Materiales')->find($actividad[$i]);
                $entity = new ProyectoApuDetalle();
                $entity->setProyectoapu($num_apu);
                $entity->setTipoActividad($tipo);
                $entity->setIdActividad($actividad[$i]);
                $entity->setCantidad(1);
                $entity->setDias($dias);
                $entity->setDescripcion($query->getNombre());
                $entity->setCostoUnidad($query->getPrecioVenta());
                $entity->setTotal(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                //$material = $em->getRepository('AppBundle:Materiales')->find($actividad);
                //$material->setPrecioVenta($costo);
                //$em->persist($material);
            }
        }else{
            
            $actividad=split(',',$actividad);
            $em = $this->getDoctrine()->getManager();
            
            for ($i=0; $i< count($actividad) ; $i++) { 
                $entity = new ProyectoApuDetalle();
                if ($tipo==2) {
                    $query = $em->getRepository('AppBundle:Maquinarias')->find($actividad[$i]);
                    $entity->setIdActividad($query->getId());
                    $entity->setDescripcion($query->getNombre());
                    $entity->setCostoUnidad($query->getPrecioDia());
                }else{
                    $query = $em->getRepository('AppBundle:ManoObra')->find($actividad[$i]);
                    $entity->setIdActividad($query->getId());
                    $entity->setDescripcion($query->getDescripcion()." ".$query->getHorarioLaboral());
                    $entity->setCostoUnidad($query->getPrecioDia());
                }

                $entity->setProyectoapu($num_apu);
                $entity->setTipoActividad($tipo);
                $entity->setCantidad(1);
                $entity->setDias(1);
                $entity->setTotal(0);
                $em->persist($entity);
            }
        }

        $em->flush();
        $query = $em->getRepository('AppBundle:ProyectoApuDetalle')->findBy(array('proyectoapu'=>$num_apu, 'tipoActividad'=> $tipo),array('descripcion' => 'ASC'));

        $content  = $this->renderView('AppBundle:ProyectoApu:detalleAjax.html.twig',array('registros'=> $query, 'tipo' => $tipo));

        $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/proyectoApuCalculos/{id_apu}-{accion}", name="proyectoapu_calculos", options={"expose"=true})
     * @Method("GET")
     */
    public function calculoDetalleApuAction(Request $request, $id_apu, $accion, $proyecto_id )
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:ProyectoApuDetalle')->findBy(array('proyectoapu'=>$id_apu),array('descripcion' => 'ASC'));
        $proyecto = $em->getRepository('AppBundle:Proyectos')->find($proyecto_id); 

       
        return $this->render('AppBundle:ProyectoApu:calculos.html.twig', array(
                'registros'=>$query,
                'accion'=>$accion,
                'proyecto'=>$proyecto,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/proyectoPrecioMaterial/{id_material}", name="proyectoapu_precio_material", options={"expose"=true})
     * @Method("GET")
     */
    public function preciosMaterialAction(Request $request, $id_material)
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Materiales')->find($id_material);
               
        return $this->render('AppBundle:ProyectoApu:preciosMaterial.html.twig', array(
            'material'=>$query,
                //'impuesto' => $this->container->getParameter('impuesto')
        ));

    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/delete/Apu/{id}", name="delete_Apu", options={"expose"=true})
     * @Method("GET")
     */
    public function deleteApuAction(Request $request, $id )
    {

        $em = $this->getDoctrine()->getManager();

        $apu = $em->getRepository('AppBundle:ProyectoAPU')->find($id); 
        $em->remove($apu);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/delete/detalleApu/{id_detalle}", name="delete_detalleApu", options={"expose"=true})
     * @Method("GET")
     */
    public function deleteDetalleApuAction(Request $request, $id_detalle )
    {

        $em = $this->getDoctrine()->getManager();

        $detalle = $em->getRepository('AppBundle:ProyectoApuDetalle')->find($id_detalle); 
        $em->remove($detalle);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
        

    }

    public function sumatoriaApu($apu_id){
        $em  = $this->getDoctrine()->getManager();
        $sql = " SELECT a.tipoActividad, 
                        b.cantApu AS repetir, 
                        SUM(a.cantidad *a.dias * costo_unidad) AS costo, 
                        COUNT(a.id) AS canti_item,
                        SUM(a.dias) AS dias,
                        SUM(a.dias*a.cantidad) AS comida
                        FROM proyectoapudetalle a, proyectoapu b
                        WHERE a.proyectoapu =$apu_id AND b.id=$apu_id
                        GROUP BY a.tipoActividad";        
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function AgregarBitacora($Accion,$referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setReferencia($referencia);
        $entity->setModulo('Item APU');
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }


    /**
     * Edits an existing Proyectos entity.
     * @Route("/cantDetailApu/{id}/{canti}", name="cantDetailApu", options={"expose"=true})
     * @Method("GET")
     */
    public function cantDetailApuAction(Request $request, $id, $canti )
    {
      $em = $this->getDoctrine()->getManager();
      $apu = $em->getRepository('AppBundle:ProyectoApuDetalle')->find($id);
      $apu->setCantidad($canti);
      $em->persist($apu);
      $em->flush();

      $response = new JsonResponse();

      return $response->setData(array(
          'status' => true,
          'content' => "listo",
          'error' => null
      ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/diasDetailApu/{id}/{dias}", name="diasDetailApu", options={"expose"=true})
     * @Method("GET")
     */
    public function diasDetailApuAction(Request $request, $id, $dias )
    {

        $em = $this->getDoctrine()->getManager();
        $apu = $em->getRepository('AppBundle:ProyectoApuDetalle')->find($id);
        $apu->setDias($dias);
        $em->persist($apu);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/ubicacionDetailApu/{id}/{valor}", name="ubicacionDetailApu", options={"expose"=true})
     * @Method("GET")
     */
    public function ubicacionDetailApuAction(Request $request, $id, $valor )
    {

        $em = $this->getDoctrine()->getManager();
        $apu = $em->getRepository('AppBundle:ProyectoApuDetalle')->find($id);
        $apu->setTipoTarea($valor);
        $em->persist($apu);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }


    /**
     * Edits an existing Proyectos entity.
     * @Route("/updatePrecio/{id_actividad}/{id_detalle}/{precio}", name="updatePrecio", options={"expose"=true})
     * @Method("GET")
     */
    public function updatePrecioAction(Request $request, $id_actividad, $id_detalle, $precio)
    {

        $em = $this->getDoctrine()->getManager();
        $apu = $em->getRepository('AppBundle:ProyectoApuDetalle')->find($id_detalle);
        $material = $em->getRepository('AppBundle:Materiales')->find($id_actividad);
        $flag = false;
        if ( $material->getPrecioVenta() <= floatval($precio)  )  {          
            $apu->setCostoUnidad($precio);
            $material->setPrecioVenta($precio);
            $em->persist($apu);
            $em->persist($material);
            $em->flush();
            $flag = true;
        }

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => $flag,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/updatePrecio2/{id_actividad}/{id_detalle}/{precio}", name="updatePrecio2", options={"expose"=true})
     * @Method("GET")
     */
    public function updatePrecio2Action(Request $request, $id_actividad, $id_detalle, $precio)
    {

        $em = $this->getDoctrine()->getManager();
        $apu = $em->getRepository('AppBundle:ProyectoApuDetalle')->find($id_detalle);
         
        $apu->setCostoUnidad($precio);
        $em->persist($apu);
        $em->flush();
       
        $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/preciosMat/{id_material}", name="preciosMat", options={"expose"=true})
     * @Method("GET")
     */
    public function preciosMatAction(Request $request, $id_material )
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Materiales')->find($id_material); 
        
        $precios=" Precio 1: ".$query->getPrecio1()."\n Precio 2: ".$query->getPrecio2()."\n Precio 3: ".$query->getPrecio3();
        $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => $precios,
                'error' => null
            ));

    }
}