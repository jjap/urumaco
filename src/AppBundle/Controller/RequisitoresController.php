<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Requisitores;
use AppBundle\Entity\Bitacora;
use AppBundle\Form\RequisitoresType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Requisitores controller.
 *
 * @Route("/admin/requisitores")
 */
class RequisitoresController extends Controller
{

    /**
     * Lists all Requisitores entities.
     *
     * @Route("/", name="requisitores")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM AppBundle:Requisitores a";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        // parameters to template
       // return $this->render('AppBundle:requisitores:lista.html.twig', );

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'admin_pool'=> $admin_pool,
            'requisitores' => $pagination,
        );
    }
    /**
     * Creates a new Requisitores entity.
     *
     * @Route("/", name="requisitores_create")
     * @Method("POST")
     * @Template("AppBundle:Requisitores:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Requisitores();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear',$entity->getRazon());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Requisitor ha sido creado con éxito.');
            ;
            return $this->redirect($this->generateUrl('requisitores_edit', array('id' => $entity->getId())));
        }

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Creates a form to create a Requisitores entity.
     *
     * @param Requisitores $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Requisitores $entity)
    {
        $form = $this->createForm(new RequisitoresType(), $entity, array(
            'action' => $this->generateUrl('requisitores_create'),
            'method' => 'POST',
        ));

        // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Requisitores entity.
     *
     * @Route("/nuevo", name="requisitores_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Requisitores();
        $form   = $this->createCreateForm($entity);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Finds and displays a Requisitores entity.
     *
     * @Route("/{id}", name="requisitores_show")
     * @Method("GET")
     * @Template()
     */
    // public function showAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('AppBundle:Requisitores')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find Requisitores entity.');
    //     }

    //     $deleteForm = $this->createDeleteForm($id);

    //     return array(
    //         'entity'      => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //     );
    // }

    /**
     * Displays a form to edit an existing Requisitores entity.
     *
     * @Route("/editar/{id}", name="requisitores_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Requisitores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Requisitores entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
    * Creates a form to edit a Requisitores entity.
    *
    * @param Requisitores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Requisitores $entity)
    {
        $form = $this->createForm(new RequisitoresType(), $entity, array(
            'action' => $this->generateUrl('requisitores_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        // $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Requisitores entity.
     *
     * @Route("/{id}", name="requisitores_update")
     * @Method("PUT")
     * @Template("AppBundle:Requisitores:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Requisitores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Requisitores entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->AgregarBitacora('Editar',$entity->getRazon());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Requisitor ha sido modificado con éxito.');
            ;
            return $this->redirect($this->generateUrl('requisitores_edit', array('id' => $id)));
        }

        $admin_pool = $this->get('sonata.admin.pool');


        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $admin_pool,
        );
    }
    /**
     * Deletes a Requisitores entity.
     *
     * @Route("/{id}", name="requisitores_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Requisitores')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Requisitores entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('requisitores'));
    }

    /**
     * Creates a form to delete a Requisitores entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('requisitores_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Deletes a Almacen entity.
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_requisitores_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:Requisitores');
            // $usuario = $this->get('security.token_storage')->getToken()->getUser()->getId();
            // $user = $this->get('security.token_storage')->getToken()->getUser();
            // $estatus_id = 0;
            // 

            $objRequestParams = json_decode($request->request->get("requestParams"));

            // die(var_dump($objRequestParams));

            // $searchParams = ( empty($request->request->get("searchParams")) ) ? null : $request->request->get("searchParams");
            // $filterParams = ( empty($request->request->get("filterParams")) ) ? null : $request->request->get("filterParams");
            //$usuario, $estatus_id,
            //

            $query = $repository->getQueryByListadoRequisitores($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $requisitores = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:Requisitores:detail-list.html.twig', array(
                'requisitores'  => $requisitores,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    public function AgregarBitacora($Accion,$referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setReferencia($referencia);
        $entity->setModulo('Requisitores');
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }
}
