<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Proyectos;
use AppBundle\Entity\Bitacora;
use AppBundle\Form\ProyectosType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\ProyectoApuDetalleExtra;
use Ob\HighchartsBundle\Highcharts\Highchart;

/**
 * Proyectos controller.
 *
 * @Route("/admin/proyectos")
 */
class ProyectosController extends Controller
{

    /*Retorna la variable admin_pool requerida en la plantilla de Sonata*/
    public function getAdmin_pool(){
        return $this->get('sonata.admin.pool');        
    }

    /**
     * Lists all Proyectos entities.
     *
     * @Route("/", name="proyectos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Proyectos')->findAll();

        return array(
            'entities' => $entities,
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }
    /**
     * Creates a new Proyectos entity.
     *
     * @Route("/", name="proyectos_create")
     * @Method("POST")
     * @Template("AppBundle:Proyectos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Proyectos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setCreadoPor($this->get('security.token_storage')->getToken()->getUser()->getUserName());
            $entity->setStatusProyectoFilter($entity->getStatusProyecto()->getName());
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear', $entity->getRq());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Proyecto ha sido creado con éxito.');
            ;
            return $this->redirect($this->generateUrl('proyectos_edit', array('id' => $entity->getId(),'admin_pool'=> $this->getAdmin_pool(),)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    /**
     * Creates a form to create a Proyectos entity.
     *
     * @param Proyectos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Proyectos $entity)
    {
        $form = $this->createForm(new ProyectosType(), $entity, array(
            'action' => $this->generateUrl('proyectos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Proyectos entity.
     *
     * @Route("/nuevo", name="proyectos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Proyectos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    // /**
    //  * Finds and displays a Proyectos entity.
    //  *
    //  * @Route("/{id}", name="proyectos_show")
    //  * @Method("GET")
    //  * @Template()
    //  */
    // public function showAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('AppBundle:Proyectos')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find Proyectos entity.');
    //     }

    //     $deleteForm = $this->createDeleteForm($id);

    //     return array(
    //         'entity'      => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //     );
    // }

    /**
     * Displays a form to edit an existing Proyectos entity.
     *
     * @Route("/{id}/editar", name="proyectos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Proyectos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proyectos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'  => $this->getAdmin_pool(),
            'suma'        => $this->sumatoria($id),
        );
    }

    /**
    * Creates a form to edit a Proyectos entity.
    *
    * @param Proyectos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Proyectos $entity)
    {
        $form = $this->createForm(new ProyectosType(), $entity, array(
            'action' => $this->generateUrl('proyectos_update', array('id' => $entity->getId(),'admin_pool'=> $this->getAdmin_pool(),)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Proyectos entity.
     *
     * @Route("/{id}", name="proyectos_update")
     * @Method("PUT")
     * @Template("AppBundle:Proyectos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Proyectos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proyectos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setStatusProyectoFilter($entity->getStatusProyecto()->getName());
            $em->flush();
            $this->AgregarBitacora('Editar', $entity->getRq());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'El Proyecto ha sido modificado con éxito.');
            ;
            return $this->redirect($this->generateUrl('proyectos_edit', array('id' => $id,'admin_pool'=> $this->getAdmin_pool(),)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
            'delete_form' => $deleteForm->createView(),
            'suma'        => $this->sumatoria($id),
        );
    }
    
    /**
     * Deletes a Proyectos entity.
     *
     * @Route("/{id}", name="proyectos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Proyectos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Proyectos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('proyectos'));
    }

    /**
     * Creates a form to delete a Proyectos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('proyectos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }


    /**
     * List Ajax All Proyectos entity.
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_proyectos_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:Proyectos');
           
            $objRequestParams = json_decode($request->request->get("requestParams"));

            $query = $repository->getQueryByListadoProyectos($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $proyectos = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:Proyectos:detail-list.html.twig', array(
                'proyectos'  => $proyectos,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    public function sumatoria($proyecto_id){
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT a.tipoActividad, SUM(a.cantidad *a.dias * costo_unidad) AS costo, COUNT(a.id) AS canti_item, SUM(a.dias) AS dias, SUM(a.dias*a.cantidad) AS comida FROM proyectoapudetalle a WHERE a.proyectoapu IN (SELECT b.id FROM proyectoapu b WHERE b.proyecto=$proyecto_id) GROUP BY a.tipoActividad";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function sumatoria_extra($proyecto_id){
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT a.tipoActividad, SUM(a.cantidad *a.dias * costo_unidad) AS costo, COUNT(a.id) AS canti_item, SUM(a.dias) AS dias, SUM(a.dias*a.cantidad) AS comida FROM proyectoapudetalle_extra a WHERE a.proyectoapu = $proyecto_id GROUP BY a.tipoActividad";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function AgregarBitacora($Accion, $referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setReferencia("RQ".$referencia);
        $entity->setModulo('Proyectos');
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     * Gestions an existing Proyectos entity.
     *
     * @Route("/gestion/{proyecto_id}", name="proyecto_gestion")
     * @Method("GET")
     * @Template("AppBundle:Proyectos:gestion.html.twig")
     */
    public function gestionAction($proyecto_id){
        $em = $this->getDoctrine()->getManager();
        $Proyecto   = $this->getDoctrine()->getRepository('AppBundle:Proyectos')->find($proyecto_id);
        $apus = $em->getRepository('AppBundle:ProyectoAPU')->findByProyecto($proyecto_id);
        $extra = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->findByProyectoapu($proyecto_id);

        $sql="SELECT d.id, e.miscelaneo as misc, a.descripcion AS Material, b.categoria_id AS categoria, c.nombre AS unidad, a.cantidad, a.costo_unidad as precioVenta FROM proyectoapudetalle a, materiales b, unidades c, proyectos d, proyectoapu e WHERE tipoActividad=3 AND a.idActividad = b.id AND b.unidad_id = c.id AND e.id=a.proyectoapu and e.proyecto=$proyecto_id group by e.id, a.idActividad";
        $sql2="SELECT c.id, c.nombre FROM categorias c WHERE c.id IN (SELECT b.categoria_id FROM proyectoapudetalle a, materiales b WHERE a.tipoActividad=3 AND a.idActividad = b.id  AND a.proyectoapu IN (SELECT e.id FROM proyectoapu e WHERE proyecto=$proyecto_id) )";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $materiales = $stmt->fetchAll();
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $categorias = $stmt->fetchAll();

        $mat= array();
        foreach ($materiales as $mate) {
            $mat[]=array(
                'nombre'  => $mate['Material'],
                'categoria'=>$mate['categoria'],
                'cantidad'=> $mate['cantidad'],
                'unidad'  => $mate['unidad'],
                'precioVenta' => $mate['precioVenta'],
                'misc'    => $mate['misc'],
                );
        }

        $cat= array();
        foreach ($categorias as $cate) {
            $cat[]=array('id'=> $cate['id'], 'nombre'=> $cate['nombre']);           
        }
        /////////////////////////////////////////////////////////////////////////////////////////////
        $sql= "SELECT c.descripcion as descrip, sum(c.cantidad * dias) as cantidad, c.costo_unidad as precioDia
                FROM proyectoapu a, proyectoapu b, proyectoapudetalle c
                where a.id = b.proyecto and b.id = c.proyectoapu and tipoActividad = 2 and a.id = $proyecto_id
                group by c.idActividad
                order by c.descripcion ASC;";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $maquinarias = $stmt->fetchAll();

        $maq= array();
        foreach ($maquinarias as $maqui) {
            $maq[]=array(
                'descrip'  => $maqui['descrip'],
                'cantidad'=> trim($maqui['cantidad']),
                'precioDia' => $maqui['precioDia'],
                );
        }
        //////////////////////////////////////////////////////////////////////////////////////////////
        $sql="SELECT c.descripcion as descrip, sum(c.cantidad * dias) as cantidad, c.costo_unidad as precioDia
                FROM proyectoapu a, proyectoapu b, proyectoapudetalle c
                where a.id = b.proyecto and b.id = c.proyectoapu and tipoActividad = 1 and a.id = $proyecto_id
                group by c.idActividad
                order by c.descripcion ASC;";
      
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $manoObra = $stmt->fetchAll();

        $mo= array();
        foreach ($manoObra as $manoO) {
            $mo[]=array(
                'descrip'  => $manoO['descrip'],
                'cantidad'=> $manoO['cantidad'],
                'precioDia' => $manoO['precioDia'],
                );
        }
        ////////////////////////////////////////////////////////////////////////////
        return $this->render('AppBundle:Proyectos:gestion.html.twig', 
            array('proyecto' => $Proyecto, 
                  'admin_pool'=> $this->getAdmin_pool(),
                  'categorias'=> $cat,
                  'manoObra' => $mo,
                  'maquinarias' => $maq,
                  'materiales'=>$mat,
                  'extras' => $extra,
            )
        );
    }

    /**
     * Add Gestions an existing Proyectos entity.
     *
     * @Route("/anexogestion/{tipo_actividad}", name="formanexo", options={"expose"=true} )
     * @Method("GET")
     * @Template("AppBundle:Proyectos:formAnexo.html.twig")
     */
    public function formAnexoAction($tipo_actividad){
      if ($tipo_actividad =="maquinaria") {
        $detalle="Agregar Maquinaria";
        $opt=1;
      }elseif ($tipo_actividad =="manoObra") {
        $detalle="Agregar Mano de Obra";
        $opt=2;
      }else{
        $detalle="Agregar Material";
        $opt=3;
      }

      $form = $this->createFormBuilder()
        ->add('manoObra', 'entity',array(
                 'class' => 'AppBundle:ManoObra',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Mano de Obra',
                 'mapped' => false,
                 'placeholder' => 'Elige una Mano de Obra',
                 'property'=>'listadoManoObra',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.descripcion');;
                 },
            ))
        ->add('costoManoObra', 'entity',array(
                 'class' => 'AppBundle:ManoObra',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Mano de Obra',
                 'mapped' => false,
                 'placeholder' => '0',
                 'property'=>'listadoCostoManoObra',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.descripcion');
                 },
            ))
        ->add('maquinaria', 'entity',array(
                 'class' => 'AppBundle:Maquinarias',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Maquinaria',
                 'mapped' => false,
                 'placeholder' => 'Elige una Maquinaria',
                 'property'=>'listadoMaquinarias',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');
                 },
            ))
        ->add('costoMaquinaria', 'entity',array(
                 'class' => 'AppBundle:Maquinarias',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Maquinaria',
                 'mapped' => false,
                 'placeholder' => '0',
                 'property'=>'listadoCostoMaquinarias',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');
                 },
            ))
        ->add('materiales', 'entity',array(
                 'class' => 'AppBundle:Materiales',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Material',
                 'mapped' => false,
                 'placeholder' => 'Elige un Material',
                 'property'=>'listadoMateriales',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');                           
                 },
            ))
        ->add('costoMateriales', 'entity',array(
                 'class' => 'AppBundle:Materiales',
                 'empty_data'  => 0,
                 'label' => 'Seleccionar Material',
                 'mapped' => false,
                 'placeholder' => '0',
                 'property'=>'listadoCostoMateriales',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');
                 },
            ))
        ->add("cantidad","text")
        ->add("costo","text")
        ->add("dia","text")
        ->getForm();


        $response = new JsonResponse();

        $content= $this->renderView("AppBundle:ProyectoApu:anexoGestion.html.twig",
                    array(
                'form'   => $form->createView(),
                'detalle'=> $detalle,
                "opt"=> $opt,
            )
         );

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/createAddAnexo/{proyecto}--{tipo}--{actividad}--{cantidad}--{costo}--{dias}--{descrip}", name="proyecto_createAddAnexo", options={"expose"=true})
     * @Method("get")
     */
    public function createAddAnexoAction(Request $request, $proyecto, $tipo, $actividad, $cantidad, $costo, $dias, $descrip )
    {

        $patrones = array();
        $patrones[0] = '/-/';
        $sustituciones = array();
        $sustituciones[0] = '/';
        $descrip = preg_replace($patrones, $sustituciones, $descrip);

        if ($tipo==3) {
            $actividad=split(',',$actividad);
            $em = $this->getDoctrine()->getManager();
            for ($i=0; $i< count($actividad) ; $i++) {
                $query = $em->getRepository('AppBundle:Materiales')->find($actividad[$i]);
                $entity = new ProyectoApuDetalleExtra();
                $entity->setProyectoapu($proyecto);
                $entity->setTipoActividad($tipo);
                $entity->setIdActividad($actividad[$i]);
                $entity->setCantidad(1);
                $entity->setDias($dias);
                $entity->setDescripcion($query->getNombre());
                $entity->setCostoUnidad($query->getPrecioVenta());
                $entity->setTotal(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
            }
        }else{
            
            $actividad=split(',',$actividad);
            $em = $this->getDoctrine()->getManager();
            
            for ($i=0; $i< count($actividad) ; $i++) { 
                $entity = new ProyectoApuDetalleExtra();
                if ($tipo==2) {
                    $query = $em->getRepository('AppBundle:Maquinarias')->find($actividad[$i]);
                    $entity->setIdActividad($query->getId());
                    $entity->setDescripcion($query->getNombre());
                    $entity->setCostoUnidad($query->getPrecioDia());
                }else{
                    $query = $em->getRepository('AppBundle:ManoObra')->find($actividad[$i]);
                    $entity->setIdActividad($query->getId());
                    $entity->setDescripcion($query->getDescripcion()." ".$query->getHorarioLaboral());
                    $entity->setCostoUnidad($query->getPrecioDia());
                }

                $entity->setProyectoapu($proyecto);
                $entity->setTipoActividad($tipo);
                $entity->setCantidad(1);
                $entity->setDias(1);
                $entity->setTotal(0);
                $em->persist($entity);
            }
        }

        $em->flush();
        $query = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->findBy(array('proyectoapu'=>$proyecto, 'tipoActividad'=> $tipo),array('descripcion' => 'ASC'));

        $content  = $this->renderView('AppBundle:ProyectoApu:detalleAjax.html.twig',array('registros'=> $query, 'tipo' => $tipo));

        $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
    }

    /**
     * @Route("/detalleApuExtra/{proyecto}-{accion}", name="detalleApuExtra_calculos", options={"expose"=true})
     * @Method("GET")
     */
    public function detalleApuExtraAction(Request $request, $proyecto, $accion )
    {

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->findBy(array('proyectoapu'=>$proyecto, 'tipoActividad'=> $accion),array('descripcion' => 'ASC'));
       
        return $this->render('AppBundle:Proyectos:calculos.html.twig', array(
                'registros'=>$query,
                'accion'=>$accion,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

    }

    /**
     * @Route("/graficos/{proyecto_id}", name="proyecto_graficos", options={"expose"=true})
     * @Method("get")
     */
    public function graficosAction(Request $request, $proyecto_id )
    {
      $em = $this->getDoctrine()->getManager();
      $proyecto = $em->getRepository('AppBundle:Proyectos')->find($proyecto_id);
      $originalValues = $this->sumatoria($proyecto_id);
      $anadidoValues  = $this->sumatoria_extra($proyecto_id);

      $serie[0] = 0;
      $serie[1] = 0;
      $serie[2] = 0;
      $serie2[0] = 0;
      $serie2[1] = 0;
      $serie2[2] = 0;

      foreach ($originalValues as $value) {
        if ($value['tipoActividad'] == 1)
          $serie[0] = (float) number_format( $value['costo'], 2, '.', '' );

        if ($value['tipoActividad'] == 2)
          $serie[1] = (float) number_format( $value['costo'], 2, '.', '' );

        if ($value['tipoActividad'] == 3)
          $serie[2] = (float) number_format( $value['costo'], 2, '.', '' );
      }

      foreach ($anadidoValues as $value) {
        if ($value['tipoActividad'] == 1)
          $serie2[0] = (float) number_format( $value['costo'], 2, '.', '' );

        if ($value['tipoActividad'] == 2)
          $serie2[1] = (float) number_format( $value['costo'], 2, '.', '' );

        if ($value['tipoActividad'] == 3)
          $serie2[2] = (float) number_format( $value['costo'], 2, '.', '' );
      }

      $serie3 = array( ($serie[0]+$serie2[0]), ($serie[1]+$serie2[1]), ($serie[2]+$serie2[2]) );

      $anadidoPie      = $this->graficoPie3D('anadidoPie' ,        $proyecto->getDescripcion(), 'Añadidos del Proyecto',      $serie2);
      $combinadoChart  = $this->graficoCombinado('combinadoChart', $proyecto->getDescripcion(), 'Estadisticas del Proyecto',  $serie, $serie2);
      $combinadoPie    = $this->graficoPie3D('combinadoPie',       $proyecto->getDescripcion(), 'Estadisticas del Proyecto',  $serie3);
      $originalPie     = $this->graficoPie3D('originalPie',        $proyecto->getDescripcion(), 'Presupuesto Inicial',        $serie);

      //die(var_dump($anadidoPie, $combinadoChart, $combinadoPie, $originalPie));
      return $this->render('AppBundle:Proyectos:graficos.html.twig', array(
        'anadidoPie'     => $anadidoPie,
        'combinadoChart' => $combinadoChart,
        'combinadoPie'   => $combinadoPie,
        'originalPie'    => $originalPie,
      ));
    }


    public function graficoPie3D($nameDiv, $title, $subtitle, $series){
      $ob = new Highchart();
      $ob->chart->renderTo($nameDiv);  // The #id of the div where to render the chart
      $ob->chart->type('pie');
      $ob->chart->options3d( array('enabled' => true, 'alpha' => 45, 'beta' => 0) );
      $ob->title->text($title);
      $ob->subtitle->text($subtitle);
      $ob->tooltip->pointFormat('Cantidad de Bs. : <b> {point.y} </b> <br/>  Porcentaje: <b>{point.percentage:.1f}%</b>');
      $ob->plotOptions->pie(
        array(
          'allowPointSelect'=> true,
          'cursor' => 'pointer',
          'dataLabels'=> array('enabled' => true, 'format' => '{point.name} <br/> {point.y} Bs.'),
          'depth' => 35,
        )
      );
      $ob->series(array(
        array(
          'type' => 'pie',
          'name' => 'Porcentaje',
          'data' => [
                ['Mano de Obra', $series[0]],
                ['Maquinarias', $series[1]],
                [
                    'name' => 'Materiales',
                    'y' => $series[2],
                    'sliced' => true,
                    'selected' => true,
                ],
            ],
        ))
      );
      return $ob;
    }

    public function graficoCombinado($nameDiv, $title, $subtitle, $serie1, $serie2){
      $ob = new Highchart();
      $ob->chart->renderTo($nameDiv);  // The #id of the div where to render the chart
      $ob->title->text($title);
      $ob->subtitle->text($subtitle);
      $ob->xAxis->categories(['Presupuesto','Añadido']);
      $ob->tooltip->pointFormat('Cantidad de Bs. : <b> {point.y} </b> <br/>  Porcentaje: <b>{point.percentage:.1f}%</b>');
      $ob->labels->items(array(
        'html' => "Titulo No se",
        'style' => array(
          'left'  => '50px',
          'top'   => '18px',
          'color' => 'Highcharts.theme && Highcharts.theme.textColor'
        )
      ));

      $ob->series(
        array(
          array(
            'type' => 'column',
            'name' => 'Mano de Obra',
            'data' => [$serie1[0], $serie2[0]],
            'dataLabels' => array('enabled' => true, 'format' => '{point.y} Bs.')
          ),
          array(
            'type' => 'column',
            'name' => 'Maquinarias',
            'data' => [$serie1[1], $serie2[1]],
            'dataLabels' => array('enabled' => true, 'format' => '{point.y} Bs.'),
          ),
          array(
            'type' => 'column',
            'name' => 'Materiales',
            'data' => [$serie1[2], $serie2[2]],
            'dataLabels' => array('enabled' => true, 'format' => '{point.y} Bs.')
          ),
        )
      );

      return $ob;
    }

    /**
     * @Route("/graficopie/{canti1}-{cant2}", name="proyecto_graficopie", options={"expose"=true})
     * @Method("get")
     */
    public function graficoPieAction(Request $request, $cant1 = 12, $cant2 = 24 ){
      // Chart
      $series = [
        array("name" => "Material",    "data" => [10,45]),
        array("name" => "Maquinarias", "data" => [30,60]),
        array("name" => "Mano Obra",   "data" => [50,80])
      ];

      $ob = new Highchart();
      $ob->chart->renderTo('columnchart');  // The #id of the div where to render the chart
      $ob->chart->type('column');
      $ob->title->text('Analisis de Costo e Inversión');
      $ob->xAxis->categories(['Presupuesto','Añadido']);
      $ob->xAxis->crosshair(true);
      $ob->yAxis->title(array('text'  => "Cantidad de Bs."));
      $ob->yAxis->min(0);
      $ob->tooltip->headerFormat('<span style="font-size:10px">{point.key}</span><table>');
      $ob->tooltip->pointFormat('<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>');
      $ob->tooltip->footerFormat('</table>');
      $ob->tooltip->shared(true);
      $ob->tooltip->useHTML(true);
      $ob->plotOptions->column(array('pointPadding'=> 0.2, 'borderWidth' => 0));

      $ob->series($series);

      return $this->render('AppBundle:Proyectos:comun_grafico.html.twig', array(
          'chart' => $ob
      ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/cantDetailApu/{id}/{canti}", name="cantDetailApuExtra", options={"expose"=true})
     * @Method("GET")
     */
    public function cantDetailApuAction(Request $request, $id, $canti )
    {
      $em = $this->getDoctrine()->getManager();
      $apuExtra = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->find($id);
      $apuExtra->setCantidad($canti);
      $em->persist($apuExtra);
      $em->flush();

      $response = new JsonResponse();

      return $response->setData(array(
          'status' => true,
          'content' => "listo",
          'error' => null
      ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/diasDetailApu/{id}/{dias}", name="diasDetailApuExtra", options={"expose"=true})
     * @Method("GET")
     */
    public function diasDetailApuAction(Request $request, $id, $dias )
    {

        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->find($id);
        $apuExtra->setDias($dias);
        $em->persist($apuExtra);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/ubicacionDetailApu/{id}/{valor}", name="ubicacionDetailApuExtra", options={"expose"=true})
     * @Method("GET")
     */
    public function ubicacionDetailApuAction(Request $request, $id, $valor )
    {

        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->find($id);
        $apuExtra->setTipoTarea($valor);
        $em->persist($apuExtra);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/updatePrecio/{id_actividad}/{id_detalle}/{precio}", name="updatePrecioExtra", options={"expose"=true})
     * @Method("GET")
     */
    public function updatePrecioAction(Request $request, $id_actividad, $id_detalle, $precio)
    {

        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->find($id_detalle);
        $material = $em->getRepository('AppBundle:Materiales')->find($id_actividad);
        $flag = false;
        if ( $material->getPrecioVenta() <= floatval($precio)  )  {          
            $apuExtra->setCostoUnidad($precio);
            $material->setPrecioVenta($precio);
            $em->persist($apuExtra);
            $em->persist($material);
            $em->flush();
            $flag = true;
        }

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => $flag,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/updatePrecio2/{id_actividad}/{id_detalle}/{precio}", name="updatePrecio2Extra", options={"expose"=true})
     * @Method("GET")
     */
    public function updatePrecio2Action(Request $request, $id_actividad, $id_detalle, $precio)
    {
        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->find($id_detalle);
         
        $apuExtra->setCostoUnidad($precio);
        $em->persist($apuExtra);
        $em->flush();
       
        $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/delete/detalleApu/{id_detalle}", name="delete_detalleApuExtra", options={"expose"=true})
     * @Method("GET")
     */
    public function deleteDetalleApuAction(Request $request, $id_detalle )
    {

        $em = $this->getDoctrine()->getManager();

        $detalle = $em->getRepository('AppBundle:ProyectoApuDetalleExtra')->find($id_detalle); 
        $em->remove($detalle);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/requisitores/{id_cliente}", name="requisitoresClientes", options={"expose"=true})
     * @Method("GET")
     */
    public function requisitoresAction($id_cliente)
    {
      $em = $this->getDoctrine()->getManager();

      $customer = $em->getRepository('AppBundle:Clientes')->find($id_cliente);
      
      $content = $this->renderView('AppBundle:Proyectos:requisitores.html.twig', array(
        'requisitores'  => $customer->getRequisitor(),
      ));

      $response = new JsonResponse();

      return $response->setData(array(
        'status' => true,
        'content' => $content,
        'error' => null
      ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/phone_requisitores/{id_requi}", name="phoneRequisitor", options={"expose"=true})
     * @Method("GET")
     */
    public function phoneRequisitorAction($id_requi)
    {
      $em = $this->getDoctrine()->getManager();

      $requisitor = $em->getRepository('AppBundle:Requisitores')->find($id_requi);

      $response = new JsonResponse();

      return $response->setData(array(
        'status' => true,
        'content' => $requisitor->getTlf(),
        'error' => null
      ));
    }
}