<?php

namespace AppBundle\Controller;

use AppBundle\Entity\NotaEntrega;
use AppBundle\Entity\DetalleNotaEntrega;
use AppBundle\Form\NotaEntregaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityRepository;

/**
 * NotaEntrega controller.
 *
 * @Route("/admin/notaentrega")
 */
class NotaEntregaController extends Controller
{

    /**
     * Lists all NotaEntrega entities.
     *
     * @Route("/", name="notaentrega")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'admin_pool'=> $admin_pool,
        );
    }
    /**
     * Creates a new NotaEntrega entity.
     *
     * @Route("/", name="notaentrega_create")
     * @Method("POST")
     * @Template("AppBundle:NotaEntrega:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new NotaEntrega();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $admin_pool = $this->get('sonata.admin.pool');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notaentrega_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'admin_pool'=> $admin_pool,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a NotaEntrega entity.
     *
     * @param NotaEntrega $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NotaEntrega $entity)
    {
        $form = $this->createForm(new NotaEntregaType(), $entity, array(
            'action' => $this->generateUrl('notaentrega_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new NotaEntrega entity.
     *
     * @Route("/new", name="notaentrega_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new NotaEntrega();
        $admin_pool = $this->get('sonata.admin.pool');
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'admin_pool'=> $admin_pool,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a NotaEntrega entity.
     *
     * @Route("/{id}", name="notaentrega_show")
     * @Method("GET")
     * @Template()
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:NotaEntrega')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaEntrega entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }
     */

    /**
     * Displays a form to edit an existing NotaEntrega entity.
     *
     * @Route("/{id}/edit", name="notaentrega_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $admin_pool = $this->get('sonata.admin.pool');

        $entity = $em->getRepository('AppBundle:NotaEntrega')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaEntrega entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'admin_pool'=> $admin_pool,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a NotaEntrega entity.
    *
    * @param NotaEntrega $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(NotaEntrega $entity)
    {
        $form = $this->createForm(new NotaEntregaType(), $entity, array(
            'action' => $this->generateUrl('notaentrega_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing NotaEntrega entity.
     *
     * @Route("/{id}", name="notaentrega_update")
     * @Method("PUT")
     * @Template("AppBundle:NotaEntrega:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $admin_pool = $this->get('sonata.admin.pool');

        $entity = $em->getRepository('AppBundle:NotaEntrega')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaEntrega entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('notaentrega_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'admin_pool'=> $admin_pool,
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a NotaEntrega entity.
     *
     * @Route("/{id}", name="notaentrega_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:NotaEntrega')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NotaEntrega entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notaentrega'));
    }

    /**
     * Creates a form to delete a NotaEntrega entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notaentrega_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_notas_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:NotaEntrega');

            $objRequestParams = json_decode($request->request->get("requestParams"));

            $query = $repository->getQueryByListadoNotaEntrega($objRequestParams);
         
            $paginator  = $this->get('knp_paginator');

            $notaEntregas = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:NotaEntrega:detail-list.html.twig', array(
                'notaEntregas'  => $notaEntregas,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/detalleNotaForm/", name="detalleNotaForm", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function detalleNotaFormAction(){
      $form = $this->createFormBuilder()
        ->add('materiales', 'entity',array(
          'class' => 'AppBundle:Materiales',
          'empty_data'  => 0,
          'multiple' => true,
          'label' => 'Seleccionar Material',
          'mapped' => false,
          'placeholder' => 'Elige un Material',
          'property'=>'listadoMateriales',
          'required' => false,
          'query_builder' => function (EntityRepository $er)
          {
            return $er
              ->createQueryBuilder('sc')
              ->groupBy('sc.id')
              ->orderBy('sc.nombre');                           
          },
        ))
        ->getForm();

        $response = new JsonResponse();

        $content= $this->renderView("AppBundle:NotaEntrega:formMaterial.html.twig",
          array(
            'form'   => $form->createView(),
          )
        );

        return $response->setData(
          array(
            'status' => true,
            'content' => $content,
            'error' => null
          )
        );
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/createDetalleNota/{nota}/{materiales}", name="createDetalleNota", options={"expose"=true})
     * @Method("get")
     */
    public function createDetalleNotaAction(Request $request, $nota, $materiales)
    {
      $aux = split("-", $materiales);
      $em = $this->getDoctrine()->getManager();
      for ($i=0; $i < count($aux) ; $i++) { 
        $material = $em->getRepository('AppBundle:Materiales')->find($aux[$i]);
        $unidad = $em->getRepository('AppBundle:Unidades')->find(1);
        $detalle = new DetalleNotaEntrega();

        $detalle->setNotaentrega($nota);
        $detalle->setCantidad(1);
        $detalle->setUnidad($material->getUnidad());
        $detalle->setDescripcion($material->getNombre());
        $detalle->setObservacion("");

        $em->persist($detalle);
      }
      $em->flush();
      $response = new JsonResponse();
      return $response->setData(
        array(
          'status' => true,
          'content' =>"",
          'error' => null
        )
      );
    }
    
    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/detalleNotaTable/{nota}", name="detalleNotaTable", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function detalleNotaTableAction($nota){     
      $em = $this->getDoctrine()->getManager();
      $detalles = $em->getRepository('AppBundle:DetalleNotaEntrega')->findByNotaentrega($nota);

      return $this->render('AppBundle:NotaEntrega:detalleNotaTable.html.twig', array(
        'registros'=>$detalles,
      ));
    }

    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/deleteNotaDetalle/{id}", name="deleteNotaDetalle", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function deleteNotaDetalleAction($id){
      $em = $this->getDoctrine()->getManager();

      $detalle = $em->getRepository('AppBundle:DetalleNotaEntrega')->find($id); 
      $em->remove($detalle);
      $em->flush();

      $response = new JsonResponse();

      return $response->setData(array(
        'status' => true,
        'content' => "listo",
        'error' => null
      ));
    }

    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/updateCantDetalle/{id}/{cantidad}", name="updateCantDetalle", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function updateCantDetalleAction($id,$cantidad){
      $em = $this->getDoctrine()->getManager();

      $detalle = $em->getRepository('AppBundle:DetalleNotaEntrega')->find($id); 
      $detalle->setCantidad($cantidad);
      $em->persist($detalle);
      $em->flush();

      $response = new JsonResponse();

      return $response->setData(array(
        'status' => true,
        'content' => "listo",
        'error' => null
      ));
    }

    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/updateObservDetalle/{id}/{observ}", name="updateObservDetalle", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function updateObservDetalleAction($id,$observ){
      $em = $this->getDoctrine()->getManager();

      $detalle = $em->getRepository('AppBundle:DetalleNotaEntrega')->find($id); 
      $detalle->setObservacion($observ);
      $em->persist($detalle);
      $em->flush();

      $response = new JsonResponse();

      return $response->setData(array(
        'status' => true,
        'content' => "listo",
        'error' => null
      ));
    }

    /**
     * Displays a form to edit an existing Proyectos entity.
     * @Route("/detalleNotaFormLibre/", name="detalleNotaFormLibre", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function detalleNotaFormLibreAction(){
      $form = $this->createFormBuilder()
        ->add('cantidad','text')
        ->add('descripcion','text')
        ->add('obser','text')
        ->add('unidades', 'entity',array(
          'class' => 'AppBundle:Unidades',
          'empty_data'  => 0,
          'multiple' => false,
          'label' => 'Seleccionar Unidad',
          'mapped' => false,
          'placeholder' => 'Elige una Unidad',
          'property'=>'listadoUnidades',
          'required' => false,
          'query_builder' => function (EntityRepository $er)
          {
            return $er
              ->createQueryBuilder('sc')
              ->groupBy('sc.id')
              ->orderBy('sc.nombre');                           
          },
        ))
        ->getForm();

        $response = new JsonResponse();

        $content= $this->renderView("AppBundle:NotaEntrega:formLibre.html.twig",
          array(
            'form'   => $form->createView(),
          )
        );

        return $response->setData(
          array(
            'status' => true,
            'content' => $content,
            'error' => null
          )
        );
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/createDetalleNotaLibre/{nota}/{cantidad}/{unidades}/{descrip}/{obser}", name="createDetalleNotaLibre", options={"expose"=true})
     * @Method("POST")
     */
    public function createDetalleNotaLibreAction(Request $request, $nota, $cantidad, $unidades, $descrip, $obser="")
    {
      $em = $this->getDoctrine()->getManager();
      $unidad = $em->getRepository('AppBundle:Unidades')->find($unidades);
      $detalle = new DetalleNotaEntrega();
      $detalle->setNotaentrega($nota);
      $detalle->setCantidad($cantidad);
      $detalle->setUnidad($unidad);
      $detalle->setDescripcion($descrip);
      if ($obser != "nn") {
        $detalle->setObservacion($obser);
      }

      $em->persist($detalle);

      $em->flush();
      $response = new JsonResponse();
      return $response->setData(
        array(
          'status' => true,
          'content' =>"",
          'error' => null
        )
      );
    }
}
