<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ManoObra;
use AppBundle\Entity\Bitacora;
use AppBundle\Form\ManoObraType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * ManoObra controller.
 *
 * @Route("/admin/manoobra")
 */
class ManoObraController extends Controller
{

    /*Retorna la variable admin_pool requerida en la plantilla de Sonata*/
    public function getAdmin_pool(){
        return $this->get('sonata.admin.pool');        
    }

    /**
     * Lists all ManoObra entities.
     *
     * @Route("/", name="manoobra")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:ManoObra')->findAll();

        return array(
            'entities' => $entities,
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    /**
     * Creates a new ManoObra entity.
     *
     * @Route("/", name="manoobra_create")
     * @Method("POST")
     * @Template("AppBundle:ManoObra:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ManoObra();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear', $entity->getDescripcion());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'La Mano de Obra ha sido creada con éxito.');
            ;
            return $this->redirect($this->generateUrl('manoobra_edit', array('id' => $entity->getId(),'admin_pool'=> $this->getAdmin_pool(),)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    /**
     * Creates a form to create a ManoObra entity.
     *
     * @param ManoObra $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ManoObra $entity)
    {
        $form = $this->createForm(new ManoObraType(), $entity, array(
            'action' => $this->generateUrl('manoobra_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ManoObra entity.
     *
     * @Route("/nuevo", name="manoobra_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ManoObra();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    // /**
    //  * Finds and displays a ManoObra entity.
    //  *
    //  * @Route("/{id}", name="manoobra_show")
    //  * @Method("GET")
    //  * @Template()
    //  */
    // public function showAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('AppBundle:ManoObra')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find ManoObra entity.');
    //     }

    //     $deleteForm = $this->createDeleteForm($id);

    //     return array(
    //         'entity'      => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //     );
    // }

    /**
     * Displays a form to edit an existing ManoObra entity.
     *
     * @Route("/{id}/editar", name="manoobra_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ManoObra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ManoObra entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $this->getAdmin_pool(),
        );
    }

    /**
    * Creates a form to edit a ManoObra entity.
    *
    * @param ManoObra $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ManoObra $entity)
    {
        $form = $this->createForm(new ManoObraType(), $entity, array(
            'action' => $this->generateUrl('manoobra_update', array('id' => $entity->getId(),'admin_pool'=> $this->getAdmin_pool(),)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ManoObra entity.
     *
     * @Route("/{id}", name="manoobra_update")
     * @Method("PUT")
     * @Template("AppBundle:ManoObra:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ManoObra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ManoObra entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->AgregarBitacora('Editar', $entity->getDescripcion());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'La Mano de Obra ha sido modificada con éxito.');
            ;
            return $this->redirect($this->generateUrl('manoobra_edit', array('id' => $id,'admin_pool'=> $this->getAdmin_pool(),)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ManoObra entity.
     *
     * @Route("/delele/{id}", name="manoobra_delete", options={"expose"=true})
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        $response = new JsonResponse();
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:ManoObra')->find($id);

        $em->remove($entity);
        $em->flush();

        return $response->setData(array(
                'status' => true,
                'error' => null
            ));
    }

    /**
     * Creates a form to delete a ManoObra entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manoobra_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

/**
     * List Ajax All ManoObra entity.
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_manoobra_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:ManoObra');
           
            $objRequestParams = json_decode($request->request->get("requestParams"));

            $query = $repository->getQueryByListadoManoObra($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $manoObra = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:ManoObra:detail-list.html.twig', array(
                'manoObra'  => $manoObra,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    public function AgregarBitacora($Accion, $referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setReferencia($referencia);
        $entity->setModulo('Mano de Obra');
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }



}
