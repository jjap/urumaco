<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Plantilla;
use AppBundle\Entity\PlantillaDetalle;
use AppBundle\Entity\ProyectoAPU;
use AppBundle\Entity\ProyectoApuDetalle;
use Doctrine\ORM\EntityRepository;
use AppBundle\Form\PlantillaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * ProyectoApu controller.
 *
 * @Route("/admin/plantillas")
 */
class PlantillaController extends Controller
{

	/*Retorna la variable admin_pool requerida en la plantilla de Sonata*/
    public function getAdmin_pool(){
        return $this->get('sonata.admin.pool');        
    }

	/**
     *
     * @Route("/", name="plantillas")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM AppBundle:Plantilla a";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
		    
		    return $this->render('AppBundle:Plantilla:index.html.twig', array(
          'admin_pool' => $this->getAdmin_pool(),
          'plantilla'  => $pagination,
        ));
    }

    /**
     * Creates a new Plantilla entity.
     *
     * @Route("/", name="plantilla_create")
     * @Method("POST")
     * @Template("AppBundle:Plantilla:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Plantilla();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'La Plantilla ha sido creada con éxito.');
            ;
            return $this->redirect($this->generateUrl('plantilla_edit', array('id' => $entity->getId())));
        }

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Creates a form to create a Plantilla entity.
     *
     * @param Plantilla $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Plantilla $entity)
    {
        $form = $this->createForm(new PlantillaType(), $entity, array(
            'action' => $this->generateUrl('plantilla_create'),
            'method' => 'POST',
        ));

        // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Plantilla entity.
     *
     * @Route("/nueva", name="plantilla_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Plantilla();
        $form   = $this->createCreateForm($entity);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Displays a form to edit an existing Plantillas entity.
     *
     * @Route("/editar/{id}", name="plantilla_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Plantilla')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Plantillas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
    * Creates a form to edit a Plantillas entity.
    *
    * @param Plantilla $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Plantilla $entity)
    {
      $form = $this->createForm(new PlantillaType(), $entity, array(
          'action' => $this->generateUrl('plantilla_update', array('id' => $entity->getId())),
          'method' => 'PUT',
      ));

      return $form;
    }

    /**
     * Edits an existing Plantillas entity.
     *
     * @Route("/{id}", name="plantilla_update")
     * @Method("PUT")
     * @Template("AppBundle:Plantilla:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Plantilla')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Plantillas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'La Plantilla ha sido modificada con éxito.');
            ;
            return $this->redirect($this->generateUrl('plantilla_edit', array('id' => $entity->getId() )));
        }

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
          'entity'      => $entity,
          'form'   => $editForm->createView(),
          'admin_pool'=> $admin_pool,
        );
    }

    /**
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_plantillas_detail_list", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:Plantilla');

            $objRequestParams = json_decode($request->request->get("requestParams"));

            $query = $repository->getQueryByListadoPlantilla($objRequestParams);
         
            $paginator  = $this->get('knp_paginator');

            $plantillas = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:Plantilla:detail-list.html.twig', array(
                'plantillas'  => $plantillas,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }
    
    /**
     * @Route("/detalleApuExtra/{plantilla}-{accion}", name="detallePlantilla_calculos", options={"expose"=true})
     * @Method("GET")
     */
    public function detallePlantillaAction(Request $request, $plantilla, $accion )
    {

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:PlantillaDetalle')->findBy(array('plantilla'=>$plantilla, 'tipoActividad'=> $accion),array('descripcion' => 'ASC'));
       
        return $this->render('AppBundle:Proyectos:calculos.html.twig', array(
                'registros'=>$query,
                'accion'=>$accion,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

    }

    /**
     * Add Gestions an existing Proyectos entity.
     *
     * @Route("/anexogestion/{tipo_actividad}", name="formanexoPlantilla", options={"expose"=true} )
     * @Method("GET")
     */
    public function formAnexoAction($tipo_actividad){
      if ($tipo_actividad =="maquinaria") {
        $detalle="Agregar Maquinaria";
        $opt=1;
      }elseif ($tipo_actividad =="manoObra") {
        $detalle="Agregar Mano de Obra";
        $opt=2;
      }else{
        $detalle="Agregar Material";
        $opt=3;
      }

      $form = $this->createFormBuilder()
        ->add('manoObra', 'entity',array(
                 'class' => 'AppBundle:ManoObra',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Mano de Obra',
                 'mapped' => false,
                 'placeholder' => 'Elige una Mano de Obra',
                 'property'=>'listadoManoObra',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.descripcion');;
                 },
            ))
        ->add('costoManoObra', 'entity',array(
                 'class' => 'AppBundle:ManoObra',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Mano de Obra',
                 'mapped' => false,
                 'placeholder' => '0',
                 'property'=>'listadoCostoManoObra',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.descripcion');
                 },
            ))
        ->add('maquinaria', 'entity',array(
                 'class' => 'AppBundle:Maquinarias',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Maquinaria',
                 'mapped' => false,
                 'placeholder' => 'Elige una Maquinaria',
                 'property'=>'listadoMaquinarias',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');
                 },
            ))
        ->add('costoMaquinaria', 'entity',array(
                 'class' => 'AppBundle:Maquinarias',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Maquinaria',
                 'mapped' => false,
                 'placeholder' => '0',
                 'property'=>'listadoCostoMaquinarias',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');
                 },
            ))
        ->add('materiales', 'entity',array(
                 'class' => 'AppBundle:Materiales',
                 'empty_data'  => 0,
                 'multiple' => true,
                 'label' => 'Seleccionar Material',
                 'mapped' => false,
                 'placeholder' => 'Elige un Material',
                 'property'=>'listadoMateriales',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');                           
                 },
            ))
        ->add('costoMateriales', 'entity',array(
                 'class' => 'AppBundle:Materiales',
                 'empty_data'  => 0,
                 'label' => 'Seleccionar Material',
                 'mapped' => false,
                 'placeholder' => '0',
                 'property'=>'listadoCostoMateriales',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');
                 },
            ))
        ->add("cantidad","text")
        ->add("costo","text")
        ->add("dia","text")
        ->getForm();


        $response = new JsonResponse();

        $content= $this->renderView("AppBundle:Plantilla:anexoGestion.html.twig",
                    array(
                'form'   => $form->createView(),
                'detalle'=> $detalle,
                "opt"=> $opt,
            )
         );

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/createAddAnexo/{proyecto}--{tipo}--{actividad}--{cantidad}--{costo}--{dias}--{descrip}", name="plantilla_createAddAnexo", options={"expose"=true})
     * @Method("get")
     */
    public function createAddAnexoAction(Request $request, $proyecto, $tipo, $actividad, $cantidad, $costo, $dias, $descrip )
    {
        $patrones = array();
        $patrones[0] = '/-/';
        $sustituciones = array();
        $sustituciones[0] = '/';
        $descrip = preg_replace($patrones, $sustituciones, $descrip);

        if ($tipo==3) {
            $actividad=split(',',$actividad);
            $em = $this->getDoctrine()->getManager();
            for ($i=0; $i< count($actividad) ; $i++) {
                $query = $em->getRepository('AppBundle:Materiales')->find($actividad[$i]);
                $entity = new PlantillaDetalle();
                $entity->setPlantilla($proyecto);
                $entity->setTipoActividad($tipo);
                $entity->setIdActividad($actividad[$i]);
                $entity->setCantidad(1);
                $entity->setDias($dias);
                $entity->setDescripcion($query->getNombre());
                $entity->setCostoUnidad($query->getPrecioVenta());
                $entity->setTotal(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
            }
        }else{
            
            $actividad=split(',',$actividad);
            $em = $this->getDoctrine()->getManager();
            
            for ($i=0; $i< count($actividad) ; $i++) { 
                $entity = new PlantillaDetalle();
                if ($tipo==2) {
                    $query = $em->getRepository('AppBundle:Maquinarias')->find($actividad[$i]);
                    $entity->setIdActividad($query->getId());
                    $entity->setDescripcion($query->getNombre());
                    $entity->setCostoUnidad($query->getPrecioDia());
                }else{
                    $query = $em->getRepository('AppBundle:ManoObra')->find($actividad[$i]);
                    $entity->setIdActividad($query->getId());
                    $entity->setDescripcion($query->getDescripcion()." ".$query->getHorarioLaboral());
                    $entity->setCostoUnidad($query->getPrecioDia());
                }

                $entity->setPlantilla($proyecto);
                $entity->setTipoActividad($tipo);
                $entity->setCantidad(1);
                $entity->setDias(1);
                $entity->setTotal(0);
                $em->persist($entity);
            }
        }

        $em->flush();
        $query = $em->getRepository('AppBundle:PlantillaDetalle')->findBy(array('plantilla'=>$proyecto, 'tipoActividad'=> $tipo),array('descripcion' => 'ASC'));

        $content  = $this->renderView('AppBundle:ProyectoApu:detalleAjax.html.twig',array('registros'=> $query, 'tipo' => $tipo));

        $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
    }

    /**
     * @Route("/detalleApuPlantilla/{proyecto}-{accion}", name="detalleApuPlantilla_calculos", options={"expose"=true})
     * @Method("GET")
     */
    public function detalleApuExtraAction(Request $request, $proyecto, $accion )
    {

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:PlantillaDetalle')->findBy(array('plantilla'=>$proyecto, 'tipoActividad'=> $accion),array('descripcion' => 'ASC'));
       
        return $this->render('AppBundle:Proyectos:calculos.html.twig', array(
                'registros'=>$query,
                'accion'=>$accion,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/cantDetailApu/{id}/{canti}", name="cantDetailApuPlantilla", options={"expose"=true})
     * @Method("GET")
     */
    public function cantDetailApuAction(Request $request, $id, $canti )
    {
      $em = $this->getDoctrine()->getManager();
      $apuExtra = $em->getRepository('AppBundle:PlantillaDetalle')->find($id);
      $apuExtra->setCantidad($canti);
      $em->persist($apuExtra);
      $em->flush();

      $response = new JsonResponse();

      return $response->setData(array(
          'status' => true,
          'content' => "listo",
          'error' => null
      ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/diasDetailApu/{id}/{dias}", name="diasDetailApuPlantilla", options={"expose"=true})
     * @Method("GET")
     */
    public function diasDetailApuAction(Request $request, $id, $dias )
    {

        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:PlantillaDetalle')->find($id);
        $apuExtra->setDias($dias);
        $em->persist($apuExtra);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/ubicacionDetailApu/{id}/{valor}", name="ubicacionDetailApuPlantilla", options={"expose"=true})
     * @Method("GET")
     */
    public function ubicacionDetailApuAction(Request $request, $id, $valor )
    {

        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:PlantillaDetalle')->find($id);
        $apuExtra->setTipoTarea($valor);
        $em->persist($apuExtra);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/updatePrecio/{id_actividad}/{id_detalle}/{precio}", name="updatePrecioPlantilla", options={"expose"=true})
     * @Method("GET")
     */
    public function updatePrecioAction(Request $request, $id_actividad, $id_detalle, $precio)
    {

        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:PlantillaDetalle')->find($id_detalle);
        $material = $em->getRepository('AppBundle:Materiales')->find($id_actividad);
        $flag = false;
        if ( $material->getPrecioVenta() <= floatval($precio)  )  {          
            $apuExtra->setCostoUnidad($precio);
            $material->setPrecioVenta($precio);
            $em->persist($apuExtra);
            $em->persist($material);
            $em->flush();
            $flag = true;
        }

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => $flag,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/updatePrecio2/{id_actividad}/{id_detalle}/{precio}", name="updatePrecio2Plantilla", options={"expose"=true})
     * @Method("GET")
     */
    public function updatePrecio2Action(Request $request, $id_actividad, $id_detalle, $precio)
    {
        $em = $this->getDoctrine()->getManager();
        $apuExtra = $em->getRepository('AppBundle:PlantillaDetalle')->find($id_detalle);
         
        $apuExtra->setCostoUnidad($precio);
        $em->persist($apuExtra);
        $em->flush();
       
        $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'error' => null
            ));
    }

    /**
     * Edits an existing Proyectos entity.
     * @Route("/delete/detalleApu/{id_detalle}", name="delete_detalleApuPlantilla", options={"expose"=true})
     * @Method("GET")
     */
    public function deleteDetalleApuAction(Request $request, $id_detalle )
    {

        $em = $this->getDoctrine()->getManager();

        $detalle = $em->getRepository('AppBundle:PlantillaDetalle')->find($id_detalle); 
        $em->remove($detalle);
        $em->flush();

       $response = new JsonResponse();

        return $response->setData(array(
                'status' => true,
                'content' => "listo",
                'error' => null
            ));
    }

    /**
     * Add Gestions an existing Proyectos entity.
     *
     * @Route("/listPlantilla/{proyecto}", name="listPlantilla", options={"expose"=true} )
     * @Method("GET")
     */
    public function listPlantillaAction($proyecto){
      $form = $this->createFormBuilder()
        ->add('plantilla', 'entity',array(
                 'class' => 'AppBundle:Plantilla',
                 'empty_data'  => 0,
                 'multiple' =>false,
                 'label' => 'Seleccionar Plantilla',
                 'mapped' => false,
                 'placeholder' => 'Elige una Plantilla',
                 'property'=>'listadoPlantillas',
                 'required' => false,
                 'query_builder' => function (EntityRepository $er)
                 {
                     return $er
                             ->createQueryBuilder('sc')
                             ->groupBy('sc.id')
                             ->orderBy('sc.nombre');;
                 },
            ))
        ->getForm();


        $response = new JsonResponse();

        $content= $this->renderView("AppBundle:Plantilla:listForm.html.twig",
                    array(
                'form'   => $form->createView(),
                'proyecto' => $proyecto,
            )
         );

        return $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));
    }

     /**
     * Add Gestions an existing Proyectos entity.
     *
     * @Route("/listPlantilla/{plantilla}/{proyecto}/", name="importPlantilla", options={"expose"=true} )
     * @Method("GET")
     */
    public function importPlantillaAction($proyecto,$plantilla)
    {
      $em = $this->getDoctrine()->getManager();
      $plantilla = $em->getRepository('AppBundle:Plantilla')->find($plantilla);
      $plantillaDetalle = $em->getRepository('AppBundle:PlantillaDetalle')->findByPlantilla($plantilla);

      $apu = new ProyectoAPU();

      $apu->setProyecto($proyecto);
      $apu->setMiscelaneo("");
      $apu->setNombre($plantilla->getNombre());
      $apu->setComentario($plantilla->getComentario());
      $apu->setCantApu($plantilla->getCantApu());
      $apu->setCantMedidaApu($plantilla->getCantMedidaApu());
      $apu->setTipoapu($plantilla->getTipoapu());
      $apu->setStatus($plantilla->getStatus());

      $em->persist($apu);
      $em->flush();

      foreach ($plantillaDetalle as $detalle) {
        $entity = new ProyectoApuDetalle();
        $entity->setProyectoapu($apu->getId());
        $entity->setTipoActividad($detalle->getTipoActividad());
        $entity->setIdActividad($detalle->getIdActividad());
        $entity->setCantidad($detalle->getCantidad());
        $entity->setDias($detalle->getDias());
        $entity->setTipoTarea($detalle->getTipoTarea());
        $entity->setDescripcion($detalle->getDescripcion());
        $entity->setCostoUnidad($detalle->getCostoUnidad());
        $entity->setTotal($detalle->getTotal());
        $em->persist($entity);
      }
      $em->flush();

      $response = new JsonResponse();

      return $response->setData(array(
              'status' => true,
              'error' => null
          ));
    }

    /**
     * Plantilla PDF
     *
     * @Route("/plantilla/{plantilla_id}", name="plantillaPDF")
     * @Method("GET")   
     */
    public function plantillaPDFAction($plantilla_id){
      $plantilla = $this->getDoctrine()->getRepository('AppBundle:Plantilla')->find($plantilla_id);
      $detallePlantilla = $this->getDoctrine()->getRepository('AppBundle:PlantillaDetalle')->findByPlantilla($plantilla_id);
      $mates = $this->query("SELECT a.id as id, b.abv as abv FROM materiales a, unidades b WHERE a.unidad_id=b.id");

      $Materiales=[];
      foreach ($mates as $mate) {
        $Materiales[$mate['id']]=$mate['abv'];
      }
      $html = $this->renderView('AppBundle:Plantilla:plantillaPDF.html.twig',array('materiales'=> $Materiales, 'ProyectoApuDetalle' => $detallePlantilla, 'Plantilla'=>$plantilla));

      return $this->mostrarPDF($html,$plantilla->getNombre().' Pdf');
    }

    public function query($sql){
      $em = $this->getDoctrine()->getManager();
      
      $stmt = $em->getConnection()->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
    }

    public function mostrarPDF($html,$filename,$orientacion = ""){

    $pdf = $this->get('white_october.tcpdf')->create();
        $pdf->SetAuthor('Ing Junior Aguilar');
        $pdf->SetTitle('Urumaco');
        $pdf->SetSubject('TCPDF ejemplo simple');
        //$pdf->SetHeaderData('../../../../../web/bundles/app/img/logo_urumaco.png', PDF_HEADER_LOGO_WIDTH);

        // $pdf->SetHeaderData('../../../../../web/bundles/app/img/logo_urumaco.png', PDF_HEADER_LOGO_WIDTH);

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
        //set margins

        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(10,5,10);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        //set auto page breaks
        //        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetAutoPageBreak(true,15);
        
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //set some language-dependent strings
        //$pdf->setLanguageArray($l);
        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('helvetica', '', 10, '', true);

        if($orientacion == ""){
           $pdf->AddPage();
       }else{
            $pdf->AddPage($orientacion);
       }
        

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        $pdf->Output($filename.'.pdf', 'I');
  }

}