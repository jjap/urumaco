<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bitacora;
use AppBundle\Form\UnidadesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Unidades controller.
 *
 * @Route("/admin/bitacora")
 */
class BitacoraController extends Controller
{

    /**
     * Lists all Unidades entities.
     *
     * @Route("/", name="bitacora")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT b FROM AppBundle:Bitacora b";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            30/*limit per page*/
        );

        // parameters to template
       // return $this->render('AppBundle:Almacen:lista.html.twig', );

        //$admin_pool = $this->get('sonata.admin.pool');
        $ip= $container->get('request')->getClientIp();

        return array(
          //  'admin_pool'=> $admin_pool,
            'ip' => $ip,
            //'marcas' => $pagination,
        );
    
    }

    /**
     * Deletes a Bitacora entity.
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_detail_list_bitacora", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:Bitacora');
            // $usuario = $this->get('security.token_storage')->getToken()->getUser()->getId();
            // $user = $this->get('security.token_storage')->getToken()->getUser();
            // $estatus_id = 0;
            // 

            $objRequestParams = json_decode($request->request->get("requestParams"));

            // die(var_dump($objRequestParams));

            // $searchParams = ( empty($request->request->get("searchParams")) ) ? null : $request->request->get("searchParams");
            // $filterParams = ( empty($request->request->get("filterParams")) ) ? null : $request->request->get("filterParams");
            //$usuario, $estatus_id,
            //

            $query = $repository->getQueryByListadoBitacoras($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $bitacoras = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                30/*limit per page*/
            );

            $content = $this->renderView('AppBundle:Bitacora:detail-list.html.twig', array(
                'bitacoras'  => $bitacoras,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }
}
