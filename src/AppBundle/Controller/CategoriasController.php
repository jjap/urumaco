<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Categorias;
use AppBundle\Entity\Bitacora;
use AppBundle\Form\CategoriasType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Categorias controller.
 *
 * @Route("/admin/categorias")
 */
class CategoriasController extends Controller
{

    /**
     * Lists all Categorias entities.
     *
     * @Route("/", name="categorias")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM AppBundle:Categorias a";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        // parameters to template
       // return $this->render('AppBundle:Almacen:lista.html.twig', );

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'admin_pool'=> $admin_pool,
            'categorias' => $pagination,
        );
    }
    /**
     * Creates a new Categorias entity.
     *
     * @Route("/", name="categorias_create")
     * @Method("POST")
     * @Template("AppBundle:Categorias:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Categorias();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->AgregarBitacora('Crear', $entity->getNombre());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'La Categoria ha sido creada con éxito.');
            ;
            return $this->redirect($this->generateUrl('categorias_edit', array('id' => $entity->getId())));
        }

        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Creates a form to create a Categorias entity.
     *
     * @param Categorias $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Categorias $entity)
    {
        $form = $this->createForm(new CategoriasType(), $entity, array(
            'action' => $this->generateUrl('categorias_create'),
            'method' => 'POST',
        ));

        // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Categorias entity.
     *
     * @Route("/nueva", name="categorias_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Categorias();
        $form   = $this->createCreateForm($entity);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
     * Finds and displays a Categorias entity.
     *
     * @Route("/{id}", name="categorias_show")
     * @Method("GET")
     * @Template()
     */
    // public function showAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('AppBundle:Categorias')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find Categorias entity.');
    //     }

    //     $deleteForm = $this->createDeleteForm($id);

    //     return array(
    //         'entity'      => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //     );
    // }

    /**
     * Displays a form to edit an existing Categorias entity.
     *
     * @Route("/editar/{id}", name="categorias_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Categorias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorias entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        $admin_pool = $this->get('sonata.admin.pool');

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $admin_pool,
        );
    }

    /**
    * Creates a form to edit a Categorias entity.
    *
    * @param Categorias $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Categorias $entity)
    {
        $form = $this->createForm(new CategoriasType(), $entity, array(
            'action' => $this->generateUrl('categorias_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        // $form->add('submit', 'submit', array('label' => 'Update'));
        return $form;
    }
    /**
     * Edits an existing Categorias entity.
     *
     * @Route("/{id}", name="categorias_update")
     * @Method("PUT")
     * @Template("AppBundle:Categorias:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Categorias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorias entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->AgregarBitacora('Editar', $entity->getNombre());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'La Categoria ha sido modificada con éxito.');
            ;
            return $this->redirect($this->generateUrl('categorias_edit', array('id' => $entity->getId())));
        }

        $admin_pool = $this->get('sonata.admin.pool');


        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'admin_pool'=> $admin_pool,
        );
    }
    /**
     * Deletes a Categorias entity.
     *
     * @Route("/{id}", name="categorias_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Categorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Categorias entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('categorias'));
    }

    /**
     * Creates a form to delete a Categorias entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorias_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     *
     * @Route("/detail-list/{page}", name="ajax_urumaco_detail_list_categorias", defaults={"page" = 1}, options={"expose"=true})
     */
    public function ajaxDetailListAction($page, Request $request ) {
 
        $response = new JsonResponse();

        try{
            $repository = $this->getDoctrine()->getRepository('AppBundle:Categorias');
            // $usuario = $this->get('security.token_storage')->getToken()->getUser()->getId();
            // $user = $this->get('security.token_storage')->getToken()->getUser();
            // $estatus_id = 0;
            // 

            $objRequestParams = json_decode($request->request->get("requestParams"));

            // die(var_dump($objRequestParams));

            // $searchParams = ( empty($request->request->get("searchParams")) ) ? null : $request->request->get("searchParams");
            // $filterParams = ( empty($request->request->get("filterParams")) ) ? null : $request->request->get("filterParams");
            //$usuario, $estatus_id,
            //

            $query = $repository->getQueryByListadoCategorias($objRequestParams);
         

            $paginator  = $this->get('knp_paginator');

            $categorias = $paginator->paginate(
                $query,
                $page,
                // $request->query->getInt('page', $page)/*page number*/,
                10/*limit per page*/
            );

            $content = $this->renderView('AppBundle:Categorias:detail-list.html.twig', array(
                'categorias'  => $categorias,
                //'impuesto' => $this->container->getParameter('impuesto')
            ));

            $response->setData(array(
                'status' => true,
                'content' => $content,
                'error' => null
            ));

        } catch(\Exception $e){

            $response->setData(array(
                'status' => false,
                'content' => null,
                'error' => "Ha ocurrido un error: " . $e->getMessage()
            ));

        }

        return $response;
    }

    public function AgregarBitacora($Accion,$referencia)
    {

        $entity = new Bitacora();

        $entity->setAccion($Accion);
        $entity->setReferencia($referencia);
        $entity->setModulo('Categorias');
        $usuario= $this->get('security.context')->getToken()->getUser()->getUserName();
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }
}
