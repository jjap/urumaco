function CargarListadoAlmacenes(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_detail_list') : url);

	ajaxRequest( route , "#listado_almacen");
}

function CargarListadoTipoapues(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_detail_list_tipoapu') : url);

	ajaxRequest( route , "#listado_tipoapu");
}

function CargarListadoCategorias(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_detail_list_categorias') : url);

	ajaxRequest( route , "#listado_categorias");
}

function CargarListadoUnidades(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_detail_list_unidades') : url);

	ajaxRequest( route , "#listado_unidades");
}

function CargarListadoMarcas(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_marcas_detail_list') : url);

	ajaxRequest( route , "#listado_marcas");
}

function CargarListadoModelos(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_modelos_detail_list') : url);

	ajaxRequest( route , "#listado_modelos");
}

function CargarListadoMaquinarias(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_maquinarias_detail_list') : url);

	ajaxRequest( route , "#listado_maquinarias");
}

function CargarListadoMateriales(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_materiales_detail_list') : url);

	ajaxRequest( route , "#listado_materiales");
}

function CargarListadoProveedores(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_proveedores_detail_list') : url);

	ajaxRequest( route , "#listado_proveedores");
}

function CargarListadoRequisitores(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_requisitores_detail_list') : url);

	ajaxRequest( route , "#listado_requisitores");
}

function CargarListadoStatusproyecto(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_statusproyecto_detail_list') : url);

	ajaxRequest( route , "#listado_statusproyecto");
}


function CargarListadoHorarioLaboral(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_horariolaboral_detail_list') : url);

	ajaxRequest( route , "#listado_horarioLaboral");
}

function CargarListadoClientes(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_clientes_detail_list') : url);

	ajaxRequest( route , "#listado_clientes");
}

function CargarListadoManoObra(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_manoobra_detail_list') : url);

	ajaxRequest( route , "#listado_manoobras");
}

function CargarListadoProyectos(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_proyectos_detail_list') : url);

	ajaxRequest( route , "#listado_proyectos");
}

function CargarListadoBitacora(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_detail_list_bitacora') : url);

	ajaxRequest( route , "#listado_Bitacora");
}

function CargarListadoNotas(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_notas_detail_list') : url);

	ajaxRequest( route , "#listado_Notas");
}

function CargarListadoPlantillas(url){
	if( typeof(url) === 'undefined') url = null;

	route = ((url === null) ? Routing.generate('ajax_urumaco_plantillas_detail_list') : url);

	ajaxRequest( route , "#listado_plantillas");
}

$(document).ready(function(){
		$('#text-search').val('');
		$(document).on('click', '.close-label', function(e){
			e.preventDefault();
			$('#text-search').val('');
			$('#tagSearchTextContainer').css('display','none');
			$('#btn-search').click();
		});

		$(document).on("keydown", "#text-search", function(e){
            if(this.value.length== 3 || this.value.length== 6 || this.value.length== 9 ){
           		$('#btn-search').click();
            }
            if(this.value.length== 0 ){
            	$(".close-label").click();
            }
        });
	});