/**
 * Created by jugalde on 12/8/2015.
 */

/**
 *
 * API que permite implementar el KNPPaginatorBundle v�a AJAX de forma gen�rica.
 * Funciona con UN (01) paginador por pagina, es decir con un solo detalle o listado a paginar. (Por Ahora)
 *
 * Para el uso de esta API basta con definir una funci�n pr�pia para la carga de datos en un archivo js incluido en la vista
 * donde se requiera paginar y hacer la llamada a la funcion AjaxRequest con sus respectivos parametros solicitados.
 *
 * Tambien se debe definir el nombre de la funci�n creada para la carga inicial de datos en una variable string
 * llamada "DataLoaderFunctionName"
 *
 * Ej: var DataLoaderFunctionName = "NombreDeMiFuncion";
 *
 */

var DataLoaderFunctionName = null;
var route = "";
var searchFields = null;
var searchText = null;
var searchParams = null;
var searchFilters = [] ;

$( document ).ready(function() {

    DataLoaderFunctionName = $( '#KnpPaginatorScript' ).attr( "DataLoaderFunctionName" );
    searchFields = $( '#KnpPaginatorScript' ).attr( "searchFields" ).split( ',' );

    executeFunctionByName( DataLoaderFunctionName, window );

    $( document ).on( "click", ".pagination li a", function( e ){
        e.preventDefault();
        GoToPage($(this).attr( "href" ));
    });

    $( document ).on( "click", "#sort_by", function( e ){
        e.preventDefault();
        executeFunctionByName( DataLoaderFunctionName, window, $( this ).attr( "href" ) );
    });

    $( document ).on( "click", '.widget .reload', function ( event ) {
        event.preventDefault();
        //searchParams = [];
        //searchText = null;
        searchParams = null;
        searchFilters = [] ;
        //$("#text-search").val("");
        $( "#tagSearchText" ).html( "" );
        $( "#tagSearchTextContainer" ).css( "display", "none" );
        executeFunctionByName( DataLoaderFunctionName, window, null );
    });

    $( document ).on( "click", '#btn-search', function ( event ) {
        event.preventDefault();
        searchText = $( "#text-search" ).val();
        setSearchParams( searchFields, searchText );
        executeFunctionByName( DataLoaderFunctionName, window, null );
        $( "#containerSearchText" ).html("");
        if( searchText !== '' ) {
            $( "#tagSearchText" ).html( searchText );
            $( "#tagSearchTextContainer" ).css( "display", "inline" );
        }
        //searchText = $( "#text-search" ).val("");
    });

    $( document ).on( "click", '#btnRemoveSearch', function ( event ) {
        searchText = "";
        searchParams = null;
        $( "#tagSearchTextContainer" ).css( "display", "none" );
        executeFunctionByName( DataLoaderFunctionName, window, null );
    });

});


function addSearchFilter ( field, value) {

    var searchFilter = [];

    if ( $.inArray( field, searchFilter ) === -1 )  {

        //Agrega el filtro en un array
        searchFilter.push( field );
        searchFilter.push( value );
        //Agrega el array del filtro en un arreglo de filtros
        searchFilters.push( searchFilter );

    }
}

function removeSearchFilters() {

    searchFilters = [];

}

function setSearchParams( searchFields, searchText ) {
    searchParams = {};
    searchParams[ "searchFields" ] = searchFields;
    searchParams[ "searchText" ] = searchText;
}

function ajaxRequest( route, appendElement ) {

    //if ( typeof( filter )=== 'undefined' ) filter = "Filter";
    //if ( typeof( sort )=== 'undefined' ) sort = "Sort";
    //if ( typeof( search )=== 'undefined' ) search = "Search";

    //var el = $( ".widget .reload" ).parents( ".widget:first" );

    var RequestParams = {};
    var jsonRequestParams = {};

    if ( searchParams !== null ) {
        RequestParams["searchParams"] = searchParams;
    } else {
        RequestParams["searchParams"] = null;
    }

    if ( searchFilters.length > 0 ) {
        RequestParams["filterParams"] =  searchFilters;
    } else {
        RequestParams["filterParams"] = null;
    }

    jsonRequestParams = JSON.stringify( RequestParams );

    $.ajax({
        method: "POST",
        url: route,
        data: { requestParams: jsonRequestParams },
        dataType: "json",
        beforeSend: function () {
            blockUI();
        }
    })
        .done(function( output ) {
            if ( output.status == true ) {
                $( appendElement ).html("").html( output.content );
                unblockUI();
            } else {
                alert(output.error );
                unblockUI( );
            }
        });

}

function executeFunctionByName(functionName, context /*, args */) {
    var args = [].slice.call(arguments).splice(2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for(var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(this, args);
}

function GoToPage( url ) {
    executeFunctionByName(DataLoaderFunctionName, window, url, searchFields);
}

function blockUI(){
    $("#opaco_div").css("display",'block');
}

function unblockUI(){
    $("#opaco_div").css("display",'none');
}
// function ShowAlerts( style, position, title, text, autohide, delay ) {

//     if ( typeof( autohide )=== 'undefined' ) autohide = false;
//     if ( typeof( delay )=== 'undefined' ) delay = 0;

//     if( style == "error" ) {
//         icon = "fa fa-exclamation";
//     } else if ( style == "warning" ){
//         icon = "fa fa-warning";
//     } else if ( style == "success" ){
//         icon = "fa fa-check";
//     } else if ( style == "info" ){
//         icon = "fa fa-question";
//     } else {
//         icon = "fa fa-circle-o";
//     }
//     $.notify({
//         title: title,
//         text: text,
//         image: "<i class='" + icon + "'></i>"
//     }, {
//         style: 'metro',
//         className: style,
//         globalPosition:position,
//         showAnimation: "show",
//         showDuration: 0,
//         hideDuration: 0,
//         autoHideDelay: delay,
//         autoHide: autohide,
//         clickToHide: true
//     });
// }


//function ParseSort( sort ) {
//    var currentSort = sort.split('?');
//    return "?" + currentSort[1];
//}

//function ToggleDisplay( element ) {
//
//    $( element ).fadeToggle( "slow" );
//    //$( element ).toggle("display", $( element ).attr("display") );
//
//}