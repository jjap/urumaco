<?php

namespace AppBundle\Transformer;

use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;

class DateToStringTransformer extends DateTimeToStringTransformer
{
    /**
     * @return array
     */
    public static function getFormats()
    {
        return self::$formats;
    }

    /**
     * @param array $formats
     */
    public static function setFormats($formats)
    {
        self::$formats = $formats;
    }

    /**
     * @return string
     */
    public function getInputTimezone()
    {
        return $this->inputTimezone;
    }

    /**
     * @param string $inputTimezone
     */
    public function setInputTimezone($inputTimezone)
    {
        $this->inputTimezone = $inputTimezone;
    }

    /**
     * @return string
     */
    public function getOutputTimezone()
    {
        return $this->outputTimezone;
    }

    /**
     * @param string $outputTimezone
     */
    public function setOutputTimezone($outputTimezone)
    {
        $this->outputTimezone = $outputTimezone;
    }

}