<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ProyectoApu
 *
 * @ORM\Table(name="proyectoapu")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProyectoApuRepository")
 */
class ProyectoAPU
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var integer
     *
     * @ORM\Column(name="proyecto", type="integer")
     */
    private $proyecto;

    /**
     * @var text
     *
     * @ORM\Column(name="miscelaneo", type="text", nullable=true)
     */
    private $miscelaneo;
    
    /**
     * @var text
     *
     * @ORM\Column(name="nombre", type="text")
     */
    private $nombre;

    /**
     * @var text
     *
     * @ORM\Column(name="comentario", type="text", nullable=true)
     */
    private $comentario;

    /**
     * @Gedmo\Slug(fields={"nombre"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantApu", type="integer")
     */
    private $cantApu;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantMedidaApu", type="integer", nullable=false)
     */
    private $cantMedidaApu;

    /**
     * @ORM\ManyToOne(targetEntity="Tipoapu", inversedBy="proyectoApu")
     * @ORM\JoinColumn(name="tipoapu_id", referencedColumnName="id", nullable=true)
     */
    private $tipoapu;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;


    public function __construct(){
        $this->cantApu= 1;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set proyecto
     *
     * @param integer $proyecto
     *
     * @return ProyectoAPU
     */
    public function setProyecto($proyecto)
    {
        $this->proyecto = $proyecto;

        return $this;
    }

    /**
     * Get proyecto
     *
     * @return integer
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    /**
     * Set miscelaneo
     *
     * @param string $miscelaneo
     *
     * @return ProyectoAPU
     */
    public function setMiscelaneo($miscelaneo)
    {
        $this->miscelaneo = strtoupper($miscelaneo);

        return $this;
    }

    /**
     * Get miscelaneo
     *
     * @return string
     */
    public function getMiscelaneo()
    {
        return $this->miscelaneo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return ProyectoAPU
     */
    public function setNombre($nombre)
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return ProyectoAPU
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set cantApu
     *
     * @param integer $cantApu
     *
     * @return ProyectoAPU
     */
    public function setCantApu($cantApu)
    {
        $this->cantApu = $cantApu;

        return $this;
    }

    /**
     * Get cantApu
     *
     * @return integer
     */
    public function getCantApu()
    {
        return $this->cantApu;
    }

    /**
     * Set cantMedidaApu
     *
     * @param integer $cantMedidaApu
     *
     * @return ProyectoAPU
     */
    public function setCantMedidaApu($cantMedidaApu)
    {
        $this->cantMedidaApu = $cantMedidaApu;

        return $this;
    }

    /**
     * Get cantApu
     *
     * @return integer
     */
    public function getCantMedidaApu()
    {
        return $this->cantMedidaApu;
    }


   
    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return ProyectoAPU
     */
    public function setComentario($comentario)
    {
        $this->comentario = strtoupper($comentario);

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set tipoapu
     *
     * @param integer $tipoapu
     *
     * @return ProyectoAPU
     */
    public function setTipoapu($tipoapu)
    {
        $this->tipoapu = $tipoapu;

        return $this;
    }

    /**
     * Get tipoapu
     *
     * @return integer
     */
    public function getTipoapu()
    {
        return $this->tipoapu;
    }

     /**
     * Set status
     *
     * @param boolean $status
     *
     * @return tipoapu
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

}
