<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Requisitores
 *
 * @ORM\Table(name="requisitores")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\RequisitoresRepository")
 */
class Requisitores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rif", type="string", length=15, unique=true, nullable = true)
     */
    private $rif;

    /**
     * @var string
     *
     * @ORM\Column(name="razon", type="string", length=150)
     */
    private $razon;

    /**
     * @var string
     *
     * @ORM\Column(name="tlf", type="string", length=100)
     */
    private $tlf;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;


    /**
     * @Gedmo\Slug(fields={"razon"}, updatable=true, separator="-")
     * @ORM\Column(length=100)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="Clientes", inversedBy="requisitor")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     *@ORM\OneToMany(targetEntity="Proyectos", mappedBy="requisitor")
     */
    private $proyecto;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proyecto = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rif
     *
     * @param string $rif
     *
     * @return Requisitores
     */
    public function setRif($rif)
    {
        $this->rif = strtoupper($rif);

        return $this;
    }

    /**
     * Get rif
     *
     * @return string
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set razon
     *
     * @param string $razon
     *
     * @return Requisitores
     */
    public function setRazon($razon)
    {
        $this->razon = strtoupper($razon);

        return $this;
    }

    /**
     * Get razon
     *
     * @return string
     */
    public function getRazon()
    {
        return $this->razon;
    }

    /**
     * Set tlf
     *
     * @param string $tlf
     *
     * @return Requisitores
     */
    public function setTlf($tlf)
    {
        $this->tlf = $tlf;

        return $this;
    }

    /**
     * Get tlf
     *
     * @return string
     */
    public function getTlf()
    {
        return $this->tlf;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Requisitores
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Requisitores
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set cliente
     *
     * @param \AppBundle\Entity\Clientes $cliente
     *
     * @return Requisitores
     */
    public function setCliente(\AppBundle\Entity\Clientes $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AppBundle\Entity\Clientes
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Add proyecto
     *
     * @param \AppBundle\Entity\Proyectos $proyecto
     *
     * @return Requisitores
     */
    public function addProyecto(\AppBundle\Entity\Proyectos $proyecto)
    {
        $this->proyecto[] = $proyecto;

        return $this;
    }

    /**
     * Remove proyecto
     *
     * @param \AppBundle\Entity\Proyectos $proyecto
     */
    public function removeProyecto(\AppBundle\Entity\Proyectos $proyecto)
    {
        $this->proyecto->removeElement($proyecto);
    }

    /**
     * Get proyecto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    public function __toString()
    {
        return $this->razon;
    }
}
