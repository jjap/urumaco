<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * NotaEntrega
 *
 * @ORM\Table(name="NotaEntrega")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\NotaEntregaRepository")
 */
class NotaEntrega
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="Proyectos", inversedBy="notaEntrega")
     * @ORM\JoinColumn(name="proyecto_id", referencedColumnName="id", nullable=false)
     */
    private $proyecto;
        
    /**
     * @var text
     *
     * @ORM\Column(name="comentario", type="text", nullable=true)
     */
    private $comentario;

    /**
     * @var string
     *
     * @ORM\Column(name="fechaEmision", type="date", nullable=true)
     */
    private $fechaEmision;

    /**
     * @var string
     *
     * @ORM\Column(name="fechaPlanta", type="date", nullable=true)
     */
    private $fechaPlanta;

    /**
     * @var string
     *
     * @ORM\Column(name="contactoUrumaco", type="string", nullable=true)
     */
    private $contactoUrumaco;

    /**
     * @var string
     *
     * @ORM\Column(name="contactoEmpresa", type="string", nullable=true)
     */
    private $contactoEmpresa;

    /**
     * @var string
     *
     * @ORM\Column(name="tlfUrumaco", type="string", nullable=true)
     */
    private $tlfUrumaco;

    /**
     * @var string
     *
     * @ORM\Column(name="tlfEmpresa", type="string", nullable=true)
     */
    private $tlfEmpresa;

    /**
     * @var string
     *
     * @ORM\Column(name="codGuia", type="string", nullable=true)
     */
    private $codGuia;

    /**
     * @var string
     *
     * @ORM\Column(name="codControl", type="string", nullable=true)
     */
    private $codControl;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set proyecto
     *
     * @param integer $proyecto
     *
     * @return NotaEntrega
     */
    public function setProyecto($proyecto)
    {
        $this->proyecto = $proyecto;

        return $this;
    }

    /**
     * Get proyecto
     *
     * @return integer
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return NotaEntrega
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set fechaEmision
     *
     * @param \DateTime $fechaEmision
     *
     * @return NotaEntrega
     */
    public function setFechaEmision($fechaEmision)
    {
        $this->fechaEmision = $fechaEmision;

        return $this;
    }

    /**
     * Get fechaEmision
     *
     * @return \DateTime
     */
    public function getFechaEmision()
    {
        return $this->fechaEmision;
    }

    /**
     * Set fechaPlanta
     *
     * @param \DateTime $fechaPlanta
     *
     * @return NotaEntrega
     */
    public function setFechaPlanta($fechaPlanta)
    {
        $this->fechaPlanta = $fechaPlanta;

        return $this;
    }

    /**
     * Get fechaPlanta
     *
     * @return \DateTime
     */
    public function getFechaPlanta()
    {
        return $this->fechaPlanta;
    }

    /**
     * Set contactoUrumaco
     *
     * @param string $contactoUrumaco
     *
     * @return NotaEntrega
     */
    public function setContactoUrumaco($contactoUrumaco)
    {
        $this->contactoUrumaco = $contactoUrumaco;

        return $this;
    }

    /**
     * Get contactoUrumaco
     *
     * @return string
     */
    public function getContactoUrumaco()
    {
        return $this->contactoUrumaco;
    }

    /**
     * Set contactoEmpresa
     *
     * @param string $contactoEmpresa
     *
     * @return NotaEntrega
     */
    public function setContactoEmpresa($contactoEmpresa)
    {
        $this->contactoEmpresa = $contactoEmpresa;

        return $this;
    }

    /**
     * Get contactoEmpresa
     *
     * @return string
     */
    public function getContactoEmpresa()
    {
        return $this->contactoEmpresa;
    }

    /**
     * Set tlfUrumaco
     *
     * @param string $tlfUrumaco
     *
     * @return NotaEntrega
     */
    public function setTlfUrumaco($tlfUrumaco)
    {
        $this->tlfUrumaco = $tlfUrumaco;

        return $this;
    }

    /**
     * Get tlfUrumaco
     *
     * @return string
     */
    public function getTlfUrumaco()
    {
        return $this->tlfUrumaco;
    }

    /**
     * Set tlfEmpresa
     *
     * @param string $tlfEmpresa
     *
     * @return NotaEntrega
     */
    public function setTlfEmpresa($tlfEmpresa)
    {
        $this->tlfEmpresa = $tlfEmpresa;

        return $this;
    }

    /**
     * Get tlfEmpresa
     *
     * @return string
     */
    public function getTlfEmpresa()
    {
        return $this->tlfEmpresa;
    }

    /**
     * Set codGuia
     *
     * @param string $codGuia
     *
     * @return NotaEntrega
     */
    public function setCodGuia($codGuia)
    {
        $this->codGuia = $codGuia;

        return $this;
    }

    /**
     * Get codGuia
     *
     * @return string
     */
    public function getCodGuia()
    {
        return $this->codGuia;
    }

    /**
     * Set codControl
     *
     * @param string $codControl
     *
     * @return NotaEntrega
     */
    public function setCodControl($codControl)
    {
        $this->codControl = $codControl;

        return $this;
    }

    /**
     * Get codControl
     *
     * @return string
     */
    public function getCodControl()
    {
        return $this->codControl;
    }

    public function __toString()
    {
        return $this->proyecto." ".$this->fechaEmision;
    }
}
