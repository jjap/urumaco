<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\NativeQuery;

/**
 * ProyectoApuRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */

class ProyectoApuRepository extends EntityRepository
{
    public function getQueryByListadoProyectoApu( $objRequestParams )
    {
        $q = $this->createQueryBuilder('proyectos');

        if( !is_null( $objRequestParams->searchParams ) ) {

            $objSearchParams = $objRequestParams->searchParams;

            $i = 1;
            foreach($objSearchParams->searchFields as $objSearchParam) {

                $like = $objSearchParam . ' LIKE :searchText';

                if($i == 1) {
                    $q->andWhere($like);
                } else {
                    $q->orWhere($like);
                }
                $i++;
            }
            $q->setParameter('searchText','%'.$objSearchParams->searchText.'%');
        }

        if ( !is_null( $objRequestParams->filterParams ) ) {
            $objFilterParams = $objRequestParams->filterParams;

            for ( $i = 0; $i < count($objFilterParams); $i++ ) {
                $q->andWhere( $objFilterParams[ $i ][ 0 ] . ' = '. $objFilterParams[ $i ][ 1 ] );
            }
        } else {
           // $q->andWhere('c.estatus IN (1,2)');
        }
    
        $q->orderBy('proyectos.id', 'DESC');
       
        return $q->getQuery();
    }

    public function getCalculos($id_apu,$tipo){

        //$apus = $em->getRepository('AppBundle:ProyectoAPU')->findByProyecto($id_apu);

        $dql = 'SELECT SUM(details.cantidad*details.costo_unidad*details.dias) FROM AppBundle:ProyectoApuDetalle details
                WHERE details.proyectoapu = :id_apu AND details.tipoActividad= :tipo';

        $query = $this->getManager()
            ->createQuery($dql)
                    ->setParameters(array('id_apu' => $id_apu, 'tipo' => $tipo));
        
        return $query->getResult();

    }

    public function Calculos($id_apu,$tipo){
        //$em    = $this->get('doctrine.orm.entity_manager');
        $dql = 'SELECT SUM(details.cantidad*details.costo_unidad*details.dias) FROM AppBundle:ProyectoApuDetalle details
                WHERE details.proyectoapu = :id_apu AND details.tipoActividad= :tipo';
        $query = $this->createQuery($dql)
                    ->setParameters(array('id_apu' => $id_apu, 'tipo' => $tipo));
        
        return $query->getResult();

    }

}