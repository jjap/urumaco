<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * HorarioLaboral
 *
 * @ORM\Table(name="horariolaboral")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\HorarioLaboralRepository")
 */
class HorarioLaboral
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @Gedmo\Slug(fields={"descripcion"}, updatable=true, separator="-")
     * @ORM\Column(length=255)
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     *@ORM\OneToMany(targetEntity="ManoObra", mappedBy="horarioLaboral")
     */
    private $manoObra;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->manoObra = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status= true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return HorarioLaboral
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = strtoupper($descripcion);

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return HorarioLaboral
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return HorarioLaboral
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add manoObra
     *
     * @param \AppBundle\Entity\ManoObra $manoObra
     *
     * @return HorarioLaboral
     */
    public function addManoObra(\AppBundle\Entity\ManoObra $manoObra)
    {
        $this->manoObra[] = $manoObra;

        return $this;
    }

    /**
     * Remove manoObra
     *
     * @param \AppBundle\Entity\ManoObra $manoObra
     */
    public function removeManoObra(\AppBundle\Entity\ManoObra $manoObra)
    {
        $this->manoObra->removeElement($manoObra);
    }

    /**
     * Get manoObra
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getManoObra()
    {
        return $this->manoObra;
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }
}
