<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Proveedores
 *
 * @ORM\Table(name="proveedores")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProveedoresRepository")
 */
class Proveedores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rif", type="string", length=15, nullable=true)
     */
    private $rif;

    /**
     * @var string
     *
     * @ORM\Column(name="razon", type="string", length=100)
     */
    private $razon;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="tlf", type="string", length=100)
     */
    private $tlf;

    /**
     * @var string
     *
     * @ORM\Column(name="tlfC", type="string", length=100, nullable=true)
     */
    private $tlfC;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="paginaWeb", type="string", length=255, nullable=true)
     */
    private $paginaWeb;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @Gedmo\Slug(fields={"razon"}, updatable=true, separator="-")
     * @ORM\Column(length=100)
     */
    private $slug;

    /**
     *@ORM\OneToMany(targetEntity="Materiales", mappedBy="proveedor1")
     */
    private $materiales1;

    /**
     *@ORM\OneToMany(targetEntity="Materiales", mappedBy="proveedor2")
     */
    private $materiales2;

    /**
     *@ORM\OneToMany(targetEntity="Materiales", mappedBy="proveedor3")
     */
    private $materiales3;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rif
     *
     * @param string $rif
     *
     * @return Proveedores
     */
    public function setRif($rif)
    {
        $this->rif = strtoupper($rif);

        return $this;
    }

    /**
     * Get rif
     *
     * @return string
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set razon
     *
     * @param string $razon
     *
     * @return Proveedores
     */
    public function setRazon($razon)
    {
        $this->razon = strtoupper($razon);

        return $this;
    }

    /**
     * Get razon
     *
     * @return string
     */
    public function getRazon()
    {
        return $this->razon;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Proveedores
     */
    public function setDireccion($direccion)
    {
        $this->direccion = strtoupper($direccion);

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set tlf
     *
     * @param string $tlf
     *
     * @return Proveedores
     */
    public function setTlf($tlf)
    {
        $this->tlf = $tlf;

        return $this;
    }

    /**
     * Get tlf
     *
     * @return string
     */
    public function getTlf()
    {
        return $this->tlf;
    }

    /**
     * Set tlfC
     *
     * @param string $tlfC
     *
     * @return Proveedores
     */
    public function setTlfC($tlfC)
    {
        $this->tlfC = $tlfC;

        return $this;
    }

    /**
     * Get tlfC
     *
     * @return string
     */
    public function getTlfC()
    {
        return $this->tlfC;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Proveedores
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set paginaWeb
     *
     * @param string $paginaWeb
     *
     * @return Proveedores
     */
    public function setPaginaWeb($paginaWeb)
    {
        $this->paginaWeb = $paginaWeb;

        return $this;
    }

    /**
     * Get paginaWeb
     *
     * @return string
     */
    public function getPaginaWeb()
    {
        return $this->paginaWeb;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Proveedores
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Proveedores
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materiales1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->materiales2 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->materiales3 = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add materiales1
     *
     * @param \AppBundle\Entity\Materiales $materiales1
     *
     * @return Proveedores
     */
    public function addMateriales1(\AppBundle\Entity\Materiales $materiales1)
    {
        $this->materiales1[] = $materiales1;

        return $this;
    }

    /**
     * Remove materiales1
     *
     * @param \AppBundle\Entity\Materiales $materiales1
     */
    public function removeMateriales1(\AppBundle\Entity\Materiales $materiales1)
    {
        $this->materiales1->removeElement($materiales1);
    }

    /**
     * Get materiales1
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMateriales1()
    {
        return $this->materiales1;
    }

    /**
     * Add materiales2
     *
     * @param \AppBundle\Entity\Materiales $materiales2
     *
     * @return Proveedores
     */
    public function addMateriales2(\AppBundle\Entity\Materiales $materiales2)
    {
        $this->materiales2[] = $materiales2;

        return $this;
    }

    /**
     * Remove materiales2
     *
     * @param \AppBundle\Entity\Materiales $materiales2
     */
    public function removeMateriales2(\AppBundle\Entity\Materiales $materiales2)
    {
        $this->materiales2->removeElement($materiales2);
    }

    /**
     * Get materiales2
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMateriales2()
    {
        return $this->materiales2;
    }

    /**
     * Add materiales3
     *
     * @param \AppBundle\Entity\Materiales $materiales3
     *
     * @return Proveedores
     */
    public function addMateriales3(\AppBundle\Entity\Materiales $materiales3)
    {
        $this->materiales3[] = $materiales3;

        return $this;
    }

    /**
     * Remove materiales3
     *
     * @param \AppBundle\Entity\Materiales $materiales3
     */
    public function removeMateriales3(\AppBundle\Entity\Materiales $materiales3)
    {
        $this->materiales3->removeElement($materiales3);
    }

    /**
     * Get materiales3
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMateriales3()
    {
        return $this->materiales3;
    }

    public function __toString()
    {
        return $this->razon;
    }
}
