<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DetalleNotaEntrega
 *
 * @ORM\Table(name="DetalleNotaEntrega")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\DetalleNotaEntregaRepository")
 */
class DetalleNotaEntrega
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="notaentrega", type="integer")
     */
    private $notaentrega;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Unidades", inversedBy="detalleNota")
     * @ORM\JoinColumn(name="unidad_id", referencedColumnName="id", nullable=true)
     */
    private $unidad;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="string", length=255, nullable=true)
     */
    private $observacion;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notaentrega
     *
     * @param integer $notaentrega
     *
     * @return DetalleNotaEntrega
     */
    public function setNotaentrega($notaentrega)
    {
        $this->notaentrega = $notaentrega;

        return $this;
    }

    /**
     * Get notaentrega
     *
     * @return integer
     */
    public function getNotaentrega()
    {
        return $this->notaentrega;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return DetalleNotaEntrega
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return DetalleNotaEntrega
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return DetalleNotaEntrega
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set unidad
     *
     * @param \AppBundle\Entity\Unidades $unidad
     *
     * @return DetalleNotaEntrega
     */
    public function setUnidad(\AppBundle\Entity\Unidades $unidad)
    {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return \AppBundle\Entity\Unidades
     */
    public function getUnidad()
    {
        return $this->unidad;
    }
}
