<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Modelos
 *
 * @ORM\Table(name="plantillaDetalle")
 * @ORM\Entity()
 */
class PlantillaDetalle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var integer
     *
     * @ORM\Column(name="plantilla", type="integer")
     */
    private $plantilla;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipoActividad", type="integer", length=50)
     */
    private $tipoActividad;

    /**
     * @var integer
     *
     * @ORM\Column(name="idActividad", type="integer", length=50)
     */
    private $idActividad;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipoTarea", type="integer", length=50)
     */
    private $tipoTarea;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float", nullable=true)
     */
    private $cantidad;

    /**
     * @var integer
     *
     * @ORM\Column(name="dias", type="integer", length=50, nullable=true)
     */
    private $dias;

    /**
     * @var float
     *
     * @ORM\Column(name="costo_unidad", type="float", nullable=true, precision=15, scale=3)
     */
    private $costo_unidad;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable=true, precision=15, scale=3)
     */
    private $total;


    /**
     * @Gedmo\Slug(fields={"descripcion"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;

    public function __construct(){
        $this->tipoTarea= 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set plantilla
     *
     * @param integer $plantilla
     *
     * @return PlantillaDetalle
     */
    public function setPlantilla($plantilla)
    {
        $this->plantilla = $plantilla;

        return $this;
    }

    /**
     * Get plantilla
     *
     * @return integer
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }

    /**
     * Set tipoActividad
     *
     * @param integer $tipoActividad
     *
     * @return PlantillaDetalle
     */
    public function setTipoActividad($tipoActividad)
    {
        $this->tipoActividad = $tipoActividad;

        return $this;
    }

    /**
     * Get tipoActividad
     *
     * @return integer
     */
    public function getTipoActividad()
    {
        return $this->tipoActividad;
    }

    /**
     * Set idActividad
     *
     * @param integer $idActividad
     *
     * @return PlantillaDetalle
     */
    public function setIdActividad($idActividad)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * Get idActividad
     *
     * @return integer
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }

    /**
     * Set tipoTarea
     *
     * @param integer $tipoTarea
     *
     * @return PlantillaDetalle
     */
    public function setTipoTarea($tipoTarea)
    {
        $this->tipoTarea = $tipoTarea;

        return $this;
    }

    /**
     * Get tipoTarea
     *
     * @return integer
     */
    public function getTipoTarea()
    {
        return $this->tipoTarea;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     *
     * @return PlantillaDetalle
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set dias
     *
     * @param integer $dias
     *
     * @return PlantillaDetalle
     */
    public function setDias($dias)
    {
        $this->dias = $dias;

        return $this;
    }

    /**
     * Get dias
     *
     * @return integer
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * Set costoUnidad
     *
     * @param float $costoUnidad
     *
     * @return PlantillaDetalle
     */
    public function setCostoUnidad($costoUnidad)
    {
        $this->costo_unidad = $costoUnidad;

        return $this;
    }

    /**
     * Get costoUnidad
     *
     * @return float
     */
    public function getCostoUnidad()
    {
        return $this->costo_unidad;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return PlantillaDetalle
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return PlantillaDetalle
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return PlantillaDetalle
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
