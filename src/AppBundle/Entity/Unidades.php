<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Unidades
 *
 * @ORM\Table(name="unidades")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UnidadesRepository")
 */
class Unidades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=20)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="abv", type="string", length=5)
     */
    private $abv;

    /**
     * @Gedmo\Slug(fields={"nombre"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;

    /**
     *@ORM\OneToMany(targetEntity="Materiales", mappedBy="unidad")
     */
    private $materiales;

    /**
     *@ORM\OneToMany(targetEntity="DetalleNotaEntrega", mappedBy="unidad")
     */
    private $detalleNota;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Unidades
     */
    public function setNombre($nombre)
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set abv
     *
     * @param string $abv
     *
     * @return Unidades
     */
    public function setAbv($abv)
    {
        $this->abv = strtoupper($abv);

        return $this;
    }

    /**
     * Get abv
     *
     * @return string
     */
    public function getAbv()
    {
        return $this->abv;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Unidades
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materiales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->detalleNota = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add materiale
     *
     * @param \AppBundle\Entity\Materiales $materiale
     *
     * @return Unidades
     */
    public function addMateriale(\AppBundle\Entity\Materiales $materiale)
    {
        $this->materiales[] = $materiale;

        return $this;
    }

    /**
     * Remove materiale
     *
     * @param \AppBundle\Entity\Materiales $materiale
     */
    public function removeMateriale(\AppBundle\Entity\Materiales $materiale)
    {
        $this->materiales->removeElement($materiale);
    }

    /**
     * Get materiales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMateriales()
    {
        return $this->materiales;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Add detalleNotum
     *
     * @param \AppBundle\Entity\DetalleNotaEntrega $detalleNotum
     *
     * @return Unidades
     */
    public function addDetalleNotum(\AppBundle\Entity\DetalleNotaEntrega $detalleNotum)
    {
        $this->detalleNota[] = $detalleNotum;

        return $this;
    }

    /**
     * Remove detalleNotum
     *
     * @param \AppBundle\Entity\DetalleNotaEntrega $detalleNotum
     */
    public function removeDetalleNotum(\AppBundle\Entity\DetalleNotaEntrega $detalleNotum)
    {
        $this->detalleNota->removeElement($detalleNotum);
    }

    /**
     * Get detalleNota
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetalleNota()
    {
        return $this->detalleNota;
    }

    public function listadoUnidades(){
        return $this->nombre;
    }
}
