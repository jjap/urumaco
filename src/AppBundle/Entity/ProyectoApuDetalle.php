<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Modelos
 *
 * @ORM\Table(name="proyectoapudetalle")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProyectoApuDetalleRepository")
 */
class ProyectoApuDetalle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var integer
     *
     * @ORM\Column(name="proyectoapu", type="integer")
     */
    private $proyectoapu;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipoActividad", type="integer", length=50)
     */
    private $tipoActividad;

    /**
     * @var integer
     *
     * @ORM\Column(name="idActividad", type="integer", length=50)
     */
    private $idActividad;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipoTarea", type="integer", length=50)
     */
    private $tipoTarea;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float", nullable=true)
     */
    private $cantidad;

    /**
     * @var integer
     *
     * @ORM\Column(name="dias", type="integer", length=50, nullable=true)
     */
    private $dias;

    /**
     * @var float
     *
     * @ORM\Column(name="costo_unidad", type="float", nullable=true, precision=15, scale=3)
     */
    private $costo_unidad;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable=true, precision=15, scale=3)
     */
    private $total;


    /**
     * @Gedmo\Slug(fields={"descripcion"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;

    public function __construct(){
        $this->tipoTarea= 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set proyectoapu
     *
     * @param integer $proyectoapu
     *
     * @return ProyectoApuDetalle
     */
    public function setProyectoapu($proyectoapu)
    {
        $this->proyectoapu = $proyectoapu;

        return $this;
    }

    /**
     * Get proyectoapu
     *
     * @return integer
     */
    public function getProyectoapu()
    {
        return $this->proyectoapu;
    }

    /**
     * Set tipoActividad
     *
     * @param integer $tipoActividad
     *
     * @return ProyectoApuDetalle
     */
    public function setTipoActividad($tipoActividad)
    {
        $this->tipoActividad = $tipoActividad;

        return $this;
    }

    /**
     * Get tipoActividad
     *
     * @return integer
     */
    public function getTipoActividad()
    {
        return $this->tipoActividad;
    }

    /**
     * Set tipoTarea
     *
     * @param integer $tipoTarea
     *
     * @return ProyectoApuDetalle
     */
    public function setTipoTarea($tipoTarea)
    {
        $this->tipoTarea = $tipoTarea;

        return $this;
    }

    /**
     * Get tipoTarea
     *
     * @return integer
     */
    public function getTipoTarea()
    {
        return $this->tipoTarea;
    }

    /**
     * Set idActividad
     *
     * @param integer $idActividad
     *
     * @return ProyectoApuDetalle
     */
    public function setIdActividad($idActividad)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * Get idActividad
     *
     * @return integer
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return ProyectoApuDetalle
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set dias
     *
     * @param integer $dias
     *
     * @return ProyectoApuDetalle
     */
    public function setDias($dias)
    {
        $this->dias = $dias;

        return $this;
    }

    /**
     * Get dias
     *
     * @return integer
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return ProyectoApuDetalle
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = strtoupper($descripcion);

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return ProyectoApuDetalle
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set costoUnidad
     *
     * @param string $costoUnidad
     *
     * @return ProyectoApuDetalle
     */
    public function setCostoUnidad($costoUnidad)
    {
        $this->costo_unidad = $costoUnidad;

        return $this;
    }

    /**
     * Get costoUnidad
     *
     * @return string
     */
    public function getCostoUnidad()
    {
        return $this->costo_unidad;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ProyectoApuDetalle
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }
}
