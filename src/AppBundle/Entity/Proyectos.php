<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Proyectos
 *
 * @ORM\Table(name="proyectos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProyectosRepository")
 */
class Proyectos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @var string
    * 
    * @ORM\Column(name="rq", type="string", length=15,nullable=true)
    */
    private $rq;

    /**
     * @var string
     *
     * @ORM\Column(name="fechaInicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="fechaEnvio", type="date", nullable=false)
     */
    private $fechaEnvio;

    /**
     * @var string
     *
     * @ORM\Column(name="fechaFin", type="date", nullable=true)
     */
    private $fechaFin;

    /**
     * @ORM\ManyToOne(targetEntity="StatusProyecto", inversedBy="proyecto")
     * @ORM\JoinColumn(name="statusProyectoId", referencedColumnName="id")
     */
    private $statusProyecto;

    /**
    * @var string
    * 
    * @ORM\Column(name="statusProyectoFilter", type="string", length=65, nullable=true, options={"default" = "EN PROCESO"})
    */
    private $statusProyectoFilter;

    /**
     * @var string
     *
     * @ORM\Column(name="creadoPor", type="string", nullable=true)
     */
    private $creadoPor;

    /**
     * @ORM\ManyToOne(targetEntity="Clientes", inversedBy="proyecto")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", nullable=false)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Requisitores", inversedBy="proyecto")
     * @ORM\JoinColumn(name="requisitor_id", referencedColumnName="id")
     */
    private $requisitor;

    /**
     * @var string
     *
     * @ORM\Column(name="oc", type="string", nullable=true)
     */
    private $oc;

    /**
     * @var string
     *
     * @ORM\Column(name="vigencia", type="string", nullable=true)
     */
    private $vigencia;

    /**
     * @var string
     *
     * @ORM\Column(name="requisitorTlf", type="string", nullable=true)
     */
    private $requisitorTlf;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text", nullable=true)
     */
    private $observacion;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="proyectos")
     * @ORM\JoinTable(name="user_proyectos")
     */
    private $asignado;
    
    /**
     * @var decimal
     *
     * @ORM\Column(name="anticipo", type="float", nullable=true, options={"default" = 0}, precision=15, scale=3)
     */
    private $anticipo;

    /**
     * @var decimal
     *
     * @ORM\Column(name="prestaciones", type="decimal", nullable=true)
     */
    private $prestaciones;

    /**
     * @var decimal
     *
     * @ORM\Column(name="gastoAdmin", type="decimal", nullable=true)
     */
    private $gastoAdmin;

    /**
     * @var decimal
     *
     * @ORM\Column(name="utilidad", type="decimal", nullable=true)
     */
    private $utilidad;

    /**
     * @var decimal
     *
     * @ORM\Column(name="comida", type="decimal", nullable=true)
     */
    private $comida;

    /**
     * @var integer
     *
     * @ORM\Column(name="diasPago", type="integer", options={"default" = 1}, nullable=true)
     */
    private $diasPago;

    /**
     * @var integer
     *
     * @ORM\Column(name="condiPago", type="integer", options={"default" = 0}, nullable=true)
     */
    private $condiPago;


    /**
     *@ORM\OneToMany(targetEntity="NotaEntrega", mappedBy="proyecto")
     */
    private $notaEntrega;

    public function __construct(){
        $this->asignado = new \Doctrine\Common\Collections\ArrayCollection();
        $this->statusProyecto = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notaEntrega = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comida=0;
        $this->gastoAdmin=0;
        $this->prestaciones=0;
        $this->utilidad=0;
        $this->anticipo=0;
        $this->diasPago=1;
        $this->condiPago=0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rq
     *
     * @param string $rq
     *
     * @return Proyectos
     */
    public function setRq($rq)
    {
        $this->rq = strtoupper($rq);

        return $this;
    }

    /**
     * Get rq
     *
     * @return string
     */
    public function getRq()
    {
        return $this->rq;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     *
     * @return Proyectos
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaEnvio
     *
     * @param \DateTime $fechaEnvio
     *
     * @return Proyectos
     */
    public function setFechaEnvio($fechaEnvio)
    {
        $this->fechaEnvio = $fechaEnvio;

        return $this;
    }

    /**
     * Get fechaEnvio
     *
     * @return \DateTime
     */
    public function getFechaEnvio()
    {
        return $this->fechaEnvio;
    }


    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     *
     * @return Proyectos
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set creadoPor
     *
     * @param string $creadoPor
     *
     * @return Proyectos
     */
    public function setCreadoPor($creadoPor)
    {
        $this->creadoPor = $creadoPor;

        return $this;
    }

    /**
     * Get creadoPor
     *
     * @return string
     */
    public function getCreadoPor()
    {
        return $this->creadoPor;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Proyectos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = strtoupper($descripcion);

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set requisitor
     *
     * @param \AppBundle\Entity\Requisitores $requisitor
     *
     * @return Proyectos
     */
    public function setRequisitor(\AppBundle\Entity\Requisitores $requisitor = null)
    {
        $this->requisitor = $requisitor;

        return $this;
    }

    /**
     * Get requisitor
     *
     * @return \AppBundle\Entity\Requisitores
     */
    public function getRequisitor()
    {
        return $this->requisitor;
    }

    /**
     * Set requisitorTlf
     *
     * @param string $requisitorTlf
     *
     * @return Proyectos
     */
    public function setRequisitorTlf($requisitorTlf)
    {
        $this->requisitorTlf = strtoupper($requisitorTlf);

        return $this;
    }

    /**
     * Get requisitorTlf
     *
     * @return string
     */
    public function getRequisitorTlf()
    {
        return $this->requisitorTlf;
    }


    /**
     * Set oc
     *
     * @param string $oc
     *
     * @return Proyectos
     */
    public function setOc($oc)
    {
        $this->oc = strtoupper($oc);

        return $this;
    }

    /**
     * Get oc
     *
     * @return string
     */
    public function getOc()
    {
        return $this->oc;
    }

    /**
     * Set vigencia
     *
     * @param string $vigencia
     *
     * @return Proyectos
     */
    public function setVigencia($vigencia)
    {
        $this->vigencia = $vigencia;

        return $this;
    }

    /**
     * Get vigencia
     *
     * @return string
     */
    public function getVigencia()
    {
        return $this->vigencia;
    }

    /**
     * Set anticipo
     *
     * @param string $anticipo
     *
     * @return Proyectos
     */
    public function setAnticipo($anticipo)
    {
        $this->anticipo = $anticipo;

        return $this;
    }

    /**
     * Get anticipo
     *
     * @return string
     */
    public function getAnticipo()
    {
        return $this->anticipo;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return Proyectos
     */
    public function setObservacion($observacion)
    {
        $this->observacion = strtoupper($observacion);

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Add asignado
     *
     * @param \Application\Sonata\UserBundle\Entity\User $asignado
     * @return Proyectos
     */
    public function addAsignado(\Application\Sonata\UserBundle\Entity\User $asignado)
    {
        $this->asignado[] = $asignado;

        return $this;
    }

    /**
     * Remove asignado
     *
     * @param \Application\Sonata\UserBundle\Entity\User $asignado
     */
    public function removeAsignado(\Application\Sonata\UserBundle\Entity\User $asignado)
    {
        $this->asignado->removeElement($asignado);
    }

    /**
     * Get asignado
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsignado()
    {
        return $this->asignado;
    }

    /**
     * Set statusProyecto
     *
     * @param \AppBundle\Entity\StatusProyecto $statusProyecto
     *
     * @return Proyectos
     */
    public function setStatusProyecto(\AppBundle\Entity\StatusProyecto $statusProyecto = null)
    {
        $this->statusProyecto = $statusProyecto;

        return $this;
    }

    /**
     * Get statusProyecto
     *
     * @return \AppBundle\Entity\StatusProyecto
     */
    public function getStatusProyecto()
    {
        return $this->statusProyecto;
    }

    /**
     * Set cliente
     *
     * @param \AppBundle\Entity\Clientes $cliente
     *
     * @return Proyectos
     */
    public function setCliente(\AppBundle\Entity\Clientes $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AppBundle\Entity\Clientes
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set prestaciones
     *
     * @param string $prestaciones
     *
     * @return Proyectos
     */
    public function setPrestaciones($prestaciones)
    {
        $this->prestaciones = $prestaciones;

        return $this;
    }

    /**
     * Get prestaciones
     *
     * @return string
     */
    public function getPrestaciones()
    {
        return $this->prestaciones;
    }

    /**
     * Set gastoAdmin
     *
     * @param string $gastoAdmin
     *
     * @return Proyectos
     */
    public function setGastoAdmin($gastoAdmin)
    {
        $this->gastoAdmin = $gastoAdmin;

        return $this;
    }

    /**
     * Get gastoAdmin
     *
     * @return string
     */
    public function getGastoAdmin()
    {
        return $this->gastoAdmin;
    }

    /**
     * Set utilidad
     *
     * @param string $utilidad
     *
     * @return Proyectos
     */
    public function setUtilidad($utilidad)
    {
        $this->utilidad = $utilidad;

        return $this;
    }

    /**
     * Get utilidad
     *
     * @return string
     */
    public function getUtilidad()
    {
        return $this->utilidad;
    }

    /**
     * Set comida
     *
     * @param string $comida
     *
     * @return Proyectos
     */
    public function setComida($comida)
    {
        $this->comida = $comida;

        return $this;
    }

    /**
     * Get comida
     *
     * @return string
     */
    public function getComida()
    {
        return $this->comida;
    }


    /**
     * Set diasPago
     *
     * @param integer $diasPago
     *
     * @return Proyectos
     */
    public function setDiasPago($diasPago)
    {
        $this->diasPago = $diasPago;

        return $this;
    }

    /**
     * Get diasPago
     *
     * @return integer
     */
    public function getDiasPago()
    {
        return $this->diasPago;
    }

    /**
     * Set condiPago
     *
     * @param integer $condiPago
     *
     * @return Proyectos
     */
    public function setCondiPago($condiPago)
    {
        $this->condiPago = $condiPago;

        return $this;
    }

    /**
     * Get condiPago
     *
     * @return integer
     */
    public function getCondiPago()
    {
        return $this->condiPago;
    }

    /**
     * Add notaEntrega
     *
     * @param \AppBundle\Entity\NotaEntrega $notaEntrega
     *
     * @return Categorias
     */
    public function addNotaEntrega(\AppBundle\Entity\NotaEntrega $notaEntrega)
    {
        $this->notaEntrega[] = $notaEntrega;

        return $this;
    }

    /**
     * Remove notaEntrega
     *
     * @param \AppBundle\Entity\NotaEntrega $notaEntrega
     */
    public function removeNotaEntrega(\AppBundle\Entity\NotaEntrega $notaEntrega)
    {
        $this->notaEntrega->removeElement($notaEntrega);
    }

    /**
     * Get notaEntrega
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotaEntregas()
    {
        return $this->notaEntrega;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    /**
     * Set statusProyectoFilter
     *
     * @param string $statusProyectoFilter
     *
     * @return Proyectos
     */
    public function setStatusProyectoFilter($statusProyectoFilter)
    {
        $this->statusProyectoFilter = $statusProyectoFilter;

        return $this;
    }

    /**
     * Get statusProyectoFilter
     *
     * @return string
     */
    public function getStatusProyectoFilter()
    {
        return $this->statusProyectoFilter;
    }

    /**
     * Get notaEntrega
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotaEntrega()
    {
        return $this->notaEntrega;
    }
}
