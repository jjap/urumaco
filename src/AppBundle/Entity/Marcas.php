<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Marcas
 *
 * @ORM\Table(name="marcas")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\MarcasRepository")
 */
class Marcas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @Gedmo\Slug(fields={"nombre"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;

    /**
     *@ORM\OneToMany(targetEntity="Maquinarias", mappedBy="marca")
     */
    private $maquinaria;

   

    public function __construct(){
        $this->maquinaria =new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Marcas
     */
    public function setNombre($nombre)
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Marcas
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add maquinarium
     *
     * @param \AppBundle\Entity\Maquinarias $maquinarium
     *
     * @return Marcas
     */
    public function addMaquinarium(\AppBundle\Entity\Maquinarias $maquinarium)
    {
        $this->maquinaria[] = $maquinarium;

        return $this;
    }

    /**
     * Remove maquinarium
     *
     * @param \AppBundle\Entity\Maquinarias $maquinarium
     */
    public function removeMaquinarium(\AppBundle\Entity\Maquinarias $maquinarium)
    {
        $this->maquinaria->removeElement($maquinarium);
    }

    /**
     * Get maquinaria
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaquinaria()
    {
        return $this->maquinaria;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}
