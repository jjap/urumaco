<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;


/**
 * ManoObra
 *
 * @ORM\Table(name="manoobra")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ManoObraRepository")
 */
class ManoObra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var float
     *
    * @ORM\Column(name="precioDia", type="float", nullable=false, options={"default" = 0}, precision=15, scale=3)
     */
    private $precioDia;

    /**
     * @ORM\ManyToOne(targetEntity="HorarioLaboral", inversedBy="manoObra")
     * @ORM\JoinColumn(name="horariolaboral_id", referencedColumnName="id", nullable=false)
     */
    private $horarioLaboral;


    /**
     * @Gedmo\Slug(fields={"descripcion"}, updatable=true, separator="-")
     * @ORM\Column(length=255)
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;


    public function __construct()
    {
        $this->status= true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return ManoObra
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = strtoupper($descripcion);

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set precioDia
     *
     * @param float $precioDia
     *
     * @return ManoObra
     */
    public function setPrecioDia($precioDia)
    {
        $this->precioDia = $precioDia;

        return $this;
    }

    /**
     * Get precioDia
     *
     * @return float
     */
    public function getPrecioDia()
    {
        return $this->precioDia;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return ManoObra
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ManoObra
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set horarioLaboral
     *
     * @param \AppBundle\Entity\HorarioLaboral $horarioLaboral
     *
     * @return ManoObra
     */
    public function setHorarioLaboral(\AppBundle\Entity\HorarioLaboral $horarioLaboral = null)
    {
        $this->horarioLaboral = $horarioLaboral;

        return $this;
    }

    /**
     * Get horarioLaboral
     *
     * @return \AppBundle\Entity\HorarioLaboral
     */
    public function getHorarioLaboral()
    {
        return $this->horarioLaboral;
    }

    public function __toString() {
        return $this->descripcion;
    }
    public function listadoManoObra(){
        return $this->descripcion. " ".$this->horarioLaboral;   
    }
    public function listadoCostoManoObra(){
        return $this->precioDia;   
    }
}
