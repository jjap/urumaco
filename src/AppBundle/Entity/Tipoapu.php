<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tipoapu
 *
 * @ORM\Table(name="tipoapu")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TipoapuRepository")
 */
class Tipoapu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @Gedmo\Slug(fields={"nombre"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;

    /**
     *@ORM\OneToMany(targetEntity="ProyectoAPU", mappedBy="tipoapu")
     */
    private $proyectoApu;

    /**
     *@ORM\OneToMany(targetEntity="Plantilla", mappedBy="tipoapu")
     */
    private $plantilla;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Tipoapu
     */
    public function setNombre($nombre)
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Tipoapu
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proyectoApu = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get proyectoApu
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProyectoAPU()
    {
        return $this->proyectoApu;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Add proyectoApu
     *
     * @param \AppBundle\Entity\ProyectoAPU $proyectoApu
     *
     * @return Tipoapu
     */
    public function addProyectoApu(\AppBundle\Entity\ProyectoAPU $proyectoApu)
    {
        $this->proyectoApu[] = $proyectoApu;

        return $this;
    }

    /**
     * Remove proyectoApu
     *
     * @param \AppBundle\Entity\ProyectoAPU $proyectoApu
     */
    public function removeProyectoApu(\AppBundle\Entity\ProyectoAPU $proyectoApu)
    {
        $this->proyectoApu->removeElement($proyectoApu);
    }

    /**
     * Add plantilla
     *
     * @param \AppBundle\Entity\Plantilla $plantilla
     *
     * @return Tipoapu
     */
    public function addPlantilla(\AppBundle\Entity\Plantilla $plantilla)
    {
        $this->plantilla[] = $plantilla;

        return $this;
    }

    /**
     * Remove plantilla
     *
     * @param \AppBundle\Entity\Plantilla $plantilla
     */
    public function removePlantilla(\AppBundle\Entity\Plantilla $plantilla)
    {
        $this->plantilla->removeElement($plantilla);
    }

    /**
     * Get plantilla
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }
}
