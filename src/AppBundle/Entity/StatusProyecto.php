<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * statusProyecto
 *
 * @ORM\Table(name="statusproyecto")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\StatusProyectoRepository")
 */
class StatusProyecto
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=100, nullable=true, options={"default" = "ccc"},)
     */
    private $color;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=true, separator="-")
     * @ORM\Column(length=200)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

     /**
     *@ORM\OneToMany(targetEntity="Proyectos", mappedBy="statusProyecto")
     */
    private $proyecto;

    public function __construct(){
        $this->color="FFFFFF";
        $this->proyecto = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StatusProyecto
     */
    public function setName($name)
    {
        $this->name = strtoupper($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return StatusProyecto
     */
    public function setColor($color)
    {
        $this->color = strtoupper($color);

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return StatusProyecto
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return StatusProyecto
     */
    public function setDescription($description)
    {
        $this->description = strtoupper($description);

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

        public function __toString()
    {
        return $this->getName();
    }

    /**
     * Add proyecto
     *
     * @param \AppBundle\Entity\Proyectos $proyecto
     *
     * @return StatusProyecto
     */
    public function addProyecto(\AppBundle\Entity\Proyectos $proyecto)
    {
        $this->proyecto[] = $proyecto;

        return $this;
    }

    /**
     * Remove proyecto
     *
     * @param \AppBundle\Entity\Proyectos $proyecto
     */
    public function removeProyecto(\AppBundle\Entity\Proyectos $proyecto)
    {
        $this->proyecto->removeElement($proyecto);
    }

    /**
     * Get proyecto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }
}