<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Materiales
 *
 * @ORM\Table(name="materiales")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\MaterialesRepository")
 */
class Materiales
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=5, nullable=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, unique=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Categorias", inversedBy="materiales")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", nullable=false)
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="Almacen", inversedBy="materiales")
     * @ORM\JoinColumn(name="almacen_id", referencedColumnName="id")
     */
    private $almacen;

    /**
     * @ORM\ManyToOne(targetEntity="Unidades", inversedBy="materiales")
     * @ORM\JoinColumn(name="unidad_id", referencedColumnName="id", nullable=false)
     */
    private $unidad;

    /**
     * @var integer
     *
     * @ORM\Column(name="usuarioCrea", type="integer", nullable=true)
     */
    private $usuarioCrea;

    /**
     * @var integer
     *
     * @ORM\Column(name="usuarioModifica", type="integer", nullable=true)
     */
    private $usuarioModifica;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var bool
     *
     * @ORM\Column(name="disponibilidad", type="boolean", nullable=true)
     */
    private $disponibilidad;

    /**
     * @ORM\ManyToOne(targetEntity="Proveedores", inversedBy="materiales1")
     * @ORM\JoinColumn(name="proveedor1_id", referencedColumnName="id")
     */
    private $proveedor1;

    /**
     * @ORM\ManyToOne(targetEntity="Proveedores", inversedBy="materiales2")
     * @ORM\JoinColumn(name="proveedor2_id", referencedColumnName="id")
     */
    private $proveedor2;

    /**
     * @ORM\ManyToOne(targetEntity="Proveedores", inversedBy="materiales3")
     * @ORM\JoinColumn(name="proveedor3_id", referencedColumnName="id")
     */
    private $proveedor3;

    /**
     * @var float
     *
     * @ORM\Column(name="precio1", type="float", nullable=false, options={"default" = 0}, precision=15, scale=3)
     */
    private $precio1;

    /**
     * @var float
     *
     * @ORM\Column(name="precio2", type="float", nullable=true, options={"default" = 0}, precision=15, scale=3)
     */
    private $precio2;

    /**
     * @var float
     *
     * @ORM\Column(name="precio3", type="float", nullable=true, options={"default" = 0}, precision=15, scale=3)
     */
    private $precio3;

    /**
     * @var float
     *
     * @ORM\Column(name="precioVenta", type="float", nullable=true, options={"default" = 0}, precision=15, scale=3)
     */
    private $precioVenta;

    /**
     * @Gedmo\Slug(fields={"nombre"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;


    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var \DateTime $contentChanged
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="change", field={"title", "body"})
     */
    private $contentChanged;


    public function __Construct(){
        $this->precioVenta = 0;
        $this->precio1 = 0;
        $this->precio2 = 0;
        $this->precio3 = 0;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Materiales
     */
    public function setCodigo($codigo)
    {
        $this->codigo = strtoupper($codigo);

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Materiales
     */
    public function setNombre($nombre)
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Materiales
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = strtoupper($descripcion);

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set categoria
     *
     * @param integer $categoria
     *
     * @return Materiales
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return integer
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set almacen
     *
     * @param integer $almacen
     *
     * @return Materiales
     */
    public function setAlmacen($almacen)
    {
        $this->almacen = $almacen;

        return $this;
    }

    /**
     * Get almacen
     *
     * @return integer
     */
    public function getAlmacen()
    {
        return $this->almacen;
    }

    /**
     * Set unidad
     *
     * @param integer $unidad
     *
     * @return Materiales
     */
    public function setUnidad($unidad)
    {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return integer
     */
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * Set usuario
     *
     * @param integer $usuario
     *
     * @return Materiales
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return integer
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set usuarioModifica
     *
     * @param integer $usuarioModifica
     *
     * @return Materiales
     */
    public function setUsuarioModifica($usuarioModifica)
    {
        $this->usuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get usuarioModifica
     *
     * @return integer
     */
    public function getUsuarioModifica()
    {
        return $this->usuarioModifica;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Materiales
     */
    public function setStock($stock)
    {
        $this->stock = strtoupper($stock);

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set disponibilidad
     *
     * @param boolean $disponibilidad
     *
     * @return Materiales
     */
    public function setDisponibilidad($disponibilidad)
    {
        $this->disponibilidad = $disponibilidad;

        return $this;
    }

    /**
     * Get disponibilidad
     *
     * @return boolean
     */
    public function getDisponibilidad()
    {
        return $this->disponibilidad;
    }

    /**
     * Set proveedor1
     *
     * @param integer $proveedor1
     *
     * @return Materiales
     */
    public function setProveedor1($proveedor1)
    {
        $this->proveedor1 = $proveedor1;

        return $this;
    }

    /**
     * Get proveedor1
     *
     * @return integer
     */
    public function getProveedor1()
    {
        return $this->proveedor1;
    }

    /**
     * Set proveedor2
     *
     * @param integer $proveedor2
     *
     * @return Materiales
     */
    public function setProveedor2($proveedor2)
    {
        $this->proveedor2 = $proveedor2;

        return $this;
    }

    /**
     * Get proveedor2
     *
     * @return integer
     */
    public function getProveedor2()
    {
        return $this->proveedor2;
    }

    /**
     * Set proveedor3
     *
     * @param integer $proveedor3
     *
     * @return Materiales
     */
    public function setProveedor3($proveedor3)
    {
        $this->proveedor3 = $proveedor3;

        return $this;
    }

    /**
     * Get proveedor3
     *
     * @return integer
     */
    public function getProveedor3()
    {
        return $this->proveedor3;
    }

    /**
     * Set precio1
     *
     * @param float $precio1
     *
     * @return Materiales
     */
    public function setPrecio1($precio1)
    {
        $this->precio1 = $precio1;

        return $this;
    }

    /**
     * Get precio1
     *
     * @return float
     */
    public function getPrecio1()
    {
        return $this->precio1;
    }

    /**
     * Set precio2
     *
     * @param float $precio2
     *
     * @return Materiales
     */
    public function setPrecio2($precio2)
    {
        $this->precio2 = $precio2;

        return $this;
    }

    /**
     * Get precio2
     *
     * @return float
     */
    public function getPrecio2()
    {
        return $this->precio2;
    }

    /**
     * Set precio3
     *
     * @param float $precio3
     *
     * @return Materiales
     */
    public function setPrecio3($precio3)
    {
        $this->precio3 = $precio3;

        return $this;
    }

    /**
     * Get precio3
     *
     * @return float
     */
    public function getPrecio3()
    {
        return $this->precio3;
    }

    /**
     * Set precioVenta
     *
     * @param float $precioVenta
     *
     * @return Materiales
     */
    public function setPrecioVenta($precioVenta)
    {
        $this->precioVenta = $precioVenta;

        return $this;
    }

    /**
     * Get precioVenta
     *
     * @return float
     */
    public function getPrecioVenta()
    {
        return $this->precioVenta;
    }



    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Materiales
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set usuarioCrea
     *
     * @param integer $usuarioCrea
     *
     * @return Materiales
     */
    public function setUsuarioCrea($usuarioCrea)
    {
        $this->usuarioCrea = $usuarioCrea;

        return $this;
    }

    /**
     * Get usuarioCrea
     *
     * @return integer
     */
    public function getUsuarioCrea()
    {
        return $this->usuarioCrea;
    }


    /**
     * Set fechaModificacion
     *
     * @param string $fechaModificacion
     *
     * @return Materiales
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return string
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    public function listadoMateriales(){
        return $this->nombre;
    }

    public function listadoCostoMateriales(){
        return $this->precioVenta;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getContentChanged()
    {
        return $this->contentChanged;
    }
}
