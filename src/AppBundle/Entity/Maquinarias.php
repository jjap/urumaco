<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Maquinarias
 *
 * @ORM\Table(name="maquinarias")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\MaquinariasRepository")
 */
class Maquinarias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=5, nullable=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=250, unique=true)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="Marcas", inversedBy="maquinaria")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id")
     */
    private $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="string", length=250, nullable=true)
     */
    private $modelo;

    /**
     * @var float
     *
     * @ORM\Column(name="precioDia", type="float", options={"default" = 0}, precision=15, scale=3)
     */
    private $precioDia;

    /**
     * @var bool
     *
     * @ORM\Column(name="disponibilidad", type="boolean", nullable=true)
     */
    private $disponibilidad;

    /**
     * @var string
     *
     * @ORM\Column(name="proyectoAcual", type="string", length=255, nullable=true)
     */
    private $proyectoAcual;

    /**
     * @Gedmo\Slug(fields={"nombre"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;


    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer", nullable=true)
     */
    private $stock;

    public function __construct(){
        $this->stock = 1;
        $this->disponibilidad=true;
    }

 /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Maquinarias
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Maquinarias
     */
    public function setCodigo($codigo)
    {
        $this->codigo = strtoupper($codigo);

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Maquinarias
     */
    public function setNombre($nombre)
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set marca
     *
     * @param integer $marca
     *
     * @return Maquinarias
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return integer
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param integer $modelo
     *
     * @return Maquinarias
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return integer
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set precioDia
     *
     * @param float $precioDia
     *
     * @return Maquinarias
     */
    public function setPrecioDia($precioDia)
    {
        $this->precioDia = $precioDia;

        return $this;
    }

    /**
     * Get precioDia
     *
     * @return float
     */
    public function getPrecioDia()
    {
        return $this->precioDia;
    }

    /**
     * Set disponibilidad
     *
     * @param boolean $disponibilidad
     *
     * @return Maquinarias
     */
    public function setDisponibilidad($disponibilidad)
    {
        $this->disponibilidad = $disponibilidad;

        return $this;
    }

    /**
     * Get disponibilidad
     *
     * @return boolean
     */
    public function getDisponibilidad()
    {
        return $this->disponibilidad;
    }

    /**
     * Set proyectoAcual
     *
     * @param string $proyectoAcual
     *
     * @return Maquinarias
     */
    public function setProyectoAcual($proyectoAcual)
    {
        $this->proyectoAcual = $proyectoAcual;

        return $this;
    }

    /**
     * Get proyectoAcual
     *
     * @return string
     */
    public function getProyectoAcual()
    {
        return $this->proyectoAcual;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Maquinarias
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function listadoMaquinarias(){
        return $this->nombre;
    }

    public function listadoCostoMaquinarias(){
        return $this->precioDia;
    }

}