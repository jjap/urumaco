<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Modelos
 *
 * @ORM\Table(name="modelos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ModelosRepository")
 */
class Modelos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @Gedmo\Slug(fields={"nombre"}, updatable=true, separator="-")
     * @ORM\Column(length=50)
     */
    private $slug;

     /**
     *@ORM\OneToMany(targetEntity="Maquinarias", mappedBy="modelo")
     */
    private $maquinarias;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Modelos
     */
    public function setNombre($nombre)
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Modelos
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->maquinarias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add maquinaria
     *
     * @param \AppBundle\Entity\Maquinarias $maquinaria
     *
     * @return Modelos
     */
    public function addMaquinaria(\AppBundle\Entity\Maquinarias $maquinaria)
    {
        $this->maquinarias[] = $maquinaria;

        return $this;
    }

    /**
     * Remove maquinaria
     *
     * @param \AppBundle\Entity\Maquinarias $maquinaria
     */
    public function removeMaquinaria(\AppBundle\Entity\Maquinarias $maquinaria)
    {
        $this->maquinarias->removeElement($maquinaria);
    }

    /**
     * Get maquinarias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaquinarias()
    {
        return $this->maquinarias;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}
