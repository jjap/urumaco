<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Clientes
 *
 * @ORM\Table(name="clientes")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ClientesRepository")
 */
class Clientes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rif", type="string", length=15, unique=true)
     */
    private $rif;

    /**
     * @var string
     *
     * @ORM\Column(name="razon", type="string", length=100)
     */
    private $razon;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="tlf", type="string", length=120)
     */
    private $tlf;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="paginaWeb", type="string", length=255, nullable=true)
     */
    private $paginaWeb;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @Gedmo\Slug(fields={"razon"}, updatable=true, separator="-")
     * @ORM\Column(length=100)
     */
    private $slug;


     /**
     *@ORM\OneToMany(targetEntity="Proyectos", mappedBy="cliente")
     */
    private $proyecto;


    /**
     *@ORM\OneToMany(targetEntity="Requisitores", mappedBy="cliente")
     */
    private $requisitor;

     /**
     * Constructor
     */
    public function __construct()
    {
        $this->proyecto = new \Doctrine\Common\Collections\ArrayCollection();
        $this->requisitor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rif
     *
     * @param string $rif
     *
     * @return Clientes
     */
    public function setRif($rif)
    {
        $this->rif = strtoupper($rif);

        return $this;
    }

    /**
     * Get rif
     *
     * @return string
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set razon
     *
     * @param string $razon
     *
     * @return Clientes
     */
    public function setRazon($razon)
    {
        $this->razon = strtoupper($razon);

        return $this;
    }

    /**
     * Get razon
     *
     * @return string
     */
    public function getRazon()
    {
        return $this->razon;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Clientes
     */
    public function setDireccion($direccion)
    {
        $this->direccion = strtoupper($direccion);

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set tlf
     *
     * @param string $tlf
     *
     * @return Clientes
     */
    public function setTlf($tlf)
    {
        $this->tlf = $tlf;

        return $this;
    }

    /**
     * Get tlf
     *
     * @return string
     */
    public function getTlf()
    {
        return $this->tlf;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Clientes
     */
    public function setEmail($email)
    {
        $this->email = strtoupper($email);

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set paginaWeb
     *
     * @param string $paginaWeb
     *
     * @return Clientes
     */
    public function setPaginaWeb($paginaWeb)
    {
        $this->paginaWeb = strtoupper($paginaWeb);

        return $this;
    }

    /**
     * Get paginaWeb
     *
     * @return string
     */
    public function getPaginaWeb()
    {
        return $this->paginaWeb;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Clientes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Clientes
     */
    public function setSlug($slug)
    {
        $this->slug = strtoupper($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add proyecto
     *
     * @param \AppBundle\Entity\Proyectos $proyecto
     *
     * @return Clientes
     */
    public function addProyecto(\AppBundle\Entity\Proyectos $proyecto)
    {
        $this->proyecto[] = $proyecto;

        return $this;
    }

    /**
     * Remove proyecto
     *
     * @param \AppBundle\Entity\Proyectos $proyecto
     */
    public function removeProyecto(\AppBundle\Entity\Proyectos $proyecto)
    {
        $this->proyecto->removeElement($proyecto);
    }

    /**
     * Get proyecto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    /**
     * Add requisitor
     *
     * @param \AppBundle\Entity\Requisitores $requisitor
     *
     * @return Clientes
     */
    public function addRequisitor(\AppBundle\Entity\Requisitores $requisitor)
    {
        $this->requisitor[] = $requisitor;

        return $this;
    }

    /**
     * Remove requisitor
     *
     * @param \AppBundle\Entity\Requisitores $requisitor
     */
    public function removeRequisitor(\AppBundle\Entity\Requisitores $requisitor)
    {
        $this->requisitor->removeElement($requisitor);
    }

    /**
     * Get requisitor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRequisitor()
    {
        return $this->requisitor;
    }

    public function __toString()
    {
        return $this->getRazon();
    }
}